<!DOCTYPE html><html>

<?php include 'parts/head.php'; ?>

<body class="article-page">

  <!-- Global Wrapper -->
  <div class="wrapper">

    <?php include 'parts/header.php'; ?>
    <?php include 'parts/main-article.php'; ?>
    <?php include 'parts/footer.php'; ?>

  </div>
  <!-- /Global Wrapper -->

  <?php include 'parts/foot.php'; ?>

</body></html>
