$(document).ready(function() {

  // Main Home Slider
  $('.home-content--slider .slider').slick({
    autoplay: true,
    infinite: true,
    speed: 600,
    easing: 'easeInOutCubic',
    prevArrow: '<button type="button" class="slick-prev slick-arrow"><i class="fa fa-play-circle"></i></button>',
    nextArrow: '<button type="button" class="slick-next slick-arrow"><i class="fa fa-play-circle"></i></button>'
  });

  // Article Slider
  $('.article-content--slider .slider').slick({
    autoplay: true,
    infinite: true,
    speed: 600,
    easing: 'easeInOutCubic',
    prevArrow: '<button type="button" class="slick-prev slick-arrow"><i class="fa fa-play-circle"></i></button>',
    nextArrow: '<button type="button" class="slick-next slick-arrow"><i class="fa fa-play-circle"></i></button>'
  });
});
