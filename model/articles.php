<?php

  $article_id = '1';
  $article_category = 'Business & Entrepreneurship';
  $article_title = 'Mengenal Prinsip Perbankan Syariah';
  $article_image = 'assets/img/article-slider/slider-1.jpg';

  $article_content_1 = '
    <p>Tidak ada cara standar mendefinisikan apa yang dimaksud dengan bank Islam / bank Syariah, adalah lembaga yang memobilisasi sumber daya keuangan dan menginvestasikannya dengan cara-cara yang ditentukan sesuai dengan ajaran Islam atau sesuai dengan tuntunan Al Qur’an dan hadist Nabi Muhammad SAW.</p>

    <p>Prinsip-prinsip keuangan Islam dipersempit ke empat konsep seperti dibawah ini:</p>

    <p><h3>1. Larangan Bunga Bank atau Riba</h3></p>

    <p>Konsep pertama dan paling penting adalah bahwa baik pemberian dan penerimaan bunga dilarang keras. Hal ini umumnya dikenal sebagai riba. Ketika Riba menggerogoti seluruh sektor ekonomi, hal itu akan  membahayakan kesejahteraan semua orang dalam komunitas tersebut. Seperti yang tercantum dalam Al Qur’an, Allah Subhanu wa Ta’ala  jelas-jelas melarang praktik riba, dalam surah Al Baqarah 275 – 276 :</p>

    <p>“Orang-orang yang memakan riba tidak dapat berdiri melainkan seperti berdirinya orang yang kemasukan setan karena gila. Yang demikian itu karena mereka berkata bahwa jual beli sama dengan riba. Padahal Allah telah menghalalkan jualbeli dan mengharamkan riba. Barangsiapa mendapat peringatan dari Tuhan nya lalu dia berhenti, maka apa yang telah diperolehnya dahulu menjadi miliknya dan urusannya terserah kepada Allah. Barangsiapa mengulangi, maka mereka itu penghuni neraka, mereka kekal didalamnya.” ( QS : 2:275 )</p>

    <p>“Allah memusnahkan keuntungan riba dan melipatgandakan keuntungan sedekah. Allah tidak menyukai setiap orang yang tetap dalam kekafiran dan bergelimang dosa.” ( QS: 2:276 )</p>

    <p>Dalam Hadist sahih Muslim, Nabi Muhammad SAW, Dari Jabir ra, ia berkata, Rasulullah SAW melaknat orang-orang yang memakan harta riba, melaknat orang yang memberi makan keluarganya dengan harta riba, melaknat penulis riba, dan saksi-saksi riba. Beliau bersabda “Semua pelaku itu hukumnya sama”. (M.22:106)</p>
  ';

  $article_content_2 = '
    <p><h3>2. Standar Etika</h3></p>

    <p>Prinsip kedua menyangkut standar etika. Ketika seorang Muslim menginvestasikan uang mereka terhadap sesuatu, secara syariah haruslah dipastikan bahwa investasi mereka adalah sesuatu yang baik dan sehat. Hal ini menunjukan bahwa investasi dalam Islam mencakup pertimbangan serius dari bisnis yang akan diinvestasikan baik dari aspek kebijakan, produk yang dihasilkannya, layanan yang disediakan, dan dampak yang dihasilkan terhadap masyarakat dan lingkungan. Dengan kata lain, umat Islam harus melihat dan terlibat langsung kedalam bisnis mereka.</p>

    <p>Dalam semua aspek dari sistem keuangan, Islam memiliki aturan-aturan tertentu, bagaimana seorang Muslim harus  terlibat dalam kegiatan ini. Misalnya, dalam perdagangan saham atau pasar sekuritas, Islam menilai apakah kegiatan perusahaan tersebut telah sesuai Syariah atau belum.</p>

    <p><h3>3. Nilai Moral dan Sosial</h3></p>

    <p>Prinsip ketiga menyangkut nilai-nilai moral dan sosial. Al-Qur\'an meminta semua penganutnya untuk berbuat baik kepada fakir miskin, seperti yang tercantum dalam Qur’an Surah Annisa ayat 36 : ....”Dan berbuat baiklah kepada kedua orang tua, karib kerabat, anak-anak yatim, orang-orang miskin..........”</p>

    <p>Lembaga keuangan Islam diharapkan untuk memberikan layanan khusus kepada mereka yang membutuhkan. Hal ini tidak terbatas pada sumbangan amal semata tetapi juga telah dilembagakan dalam industri dalam bentuk pinjaman tanpa bunga.</p>

    <p>Bisnis bank syariah meliputi proyek-proyek sosial tertentu, serta sumbangan amal. Bank syariah memberikan pinjaman bebas bunga. Sebagai contoh, jika seorang individu perlu perawatan di rumah sakit atau ingin masuk universitas, Bank Islam memberikan apa yang disebut pinjaman tanpa bunga dan biasanya diberikan untuk jangka waktu singkat satu tahun.</p>

    <p><h3>4. Kewajiban dan Risiko Bisnis</h3></p>

    <p>Prinsip terakhir menyangkut konsep menyeluruh keadilan,  bahwa semua pihak harus berbagi risiko dan keuntungan dari usaha apapun. Penyedia jasa keuangan harus bisa menerima risiko bisnis atau menyediakan beberapa layanan seperti penyediaan aset, jika dana tersebut, dari sudut pandang Syariah tidak bertentangan. Dengan menghubungkan laba dengan kemungkinan kerugian, hukum Islam membedakan keuntungan yang sah dari semua bentuk lain dari keuntungan.</p>

    <p>Dalam rangka untuk memastikan bahwa prinsip-prinsip ini diikuti, masing-masing lembaga Islam harus membangun dan menyediakan sendiri dewan penasehat yang dikenal sebagai Dewan Syariah.  Para anggota Dewan Syariah dapat mencakup bankir, pengacara atau ahli agama selama mereka dilatih dalam hukum Islam, atau Syariah. (Feb2016)</p>

    <p><i>Narasumber: Wempy Dyocta Koto</i></p>
  ';

  $most_view = array(
    array (
      'id' => '1',
      'image' => 'article-1.jpg',
      'category' => 'Spiritual',
      'title' => 'Menyikapi Perayaan Tahun Baru Masehi',
      'url' => '#'
    ),
    array (
      'id' => '2',
      'image' => 'article-2.jpg',
      'category' => 'Home Support',
      'title' => 'Menyikapi Perayaan Tahun Baru Masehi',
      'url' => '#'
    ),
    array (
      'id' => '3',
      'image' => 'article-3.jpg',
      'category' => 'Family',
      'title' => 'Menyikapi Perayaan Tahun Baru Masehi',
      'url' => '#'
    ),
    array (
      'id' => '4',
      'image' => 'article-4.jpg',
      'category' => 'Women Empowerment',
      'title' => 'Menyikapi Perayaan Tahun Baru Masehi',
      'url' => '#'
    ),
    array (
      'id' => '5',
      'image' => 'article-5.jpg',
      'category' => 'Article',
      'title' => 'Menyikapi Perayaan Tahun Baru Masehi',
      'url' => '#'
    ),
    array (
      'id' => '6',
      'image' => 'article-6.jpg',
      'category' => 'Family',
      'title' => 'Menyikapi Perayaan Tahun Baru Masehi',
      'url' => '#'
    ),
  );

  $latest_news = array(
    array (
      'id' => '1',
      'image' => 'article-1.jpg',
      'category' => 'Family',
      'title' => 'Seorang Wanita pun Bisa Memberi Nasihat',
      'url' => '#'
    ),
    array (
      'id' => '2',
      'image' => 'article-2.jpg',
      'category' => 'Fashion',
      'title' => 'Trend Busana Muslimah 2016 Versi Dian Pelangi',
      'url' => '#'
    ),
    array (
      'id' => '3',
      'image' => 'article-3.jpg',
      'category' => 'Spiritual',
      'title' => 'Membakar Semangat Menuntut Ilmu Syar\'i',
      'url' => '#'
    ),
    array (
      'id' => '4',
      'image' => 'article-4.jpg',
      'category' => 'Parenting',
      'title' => 'Cara Mendidik Anak Dalam Islam',
      'url' => '#'
    )
  );

  $latest_articles = array(
    array (
      'id' => '1',
      'image' => 'article-1.jpg',
      'category' => 'Wira usaha',
      'title' => 'Usaha Pakaian Muslim Sedang Menjamur',
      'url' => '#'
    ),
    array (
      'id' => '2',
      'image' => 'article-2.jpg',
      'category' => 'Tokoh Inspirasional',
      'title' => 'Bisnis Sukses ala Laudya Cinthya Bella',
      'url' => '#'
    ),
    array (
      'id' => '3',
      'image' => 'article-3.jpg',
      'category' => 'Wira usaha',
      'title' => 'Bisnis Secara Islam',
      'url' => '#'
    ),
    array (
      'id' => '4',
      'image' => 'article-4.jpg',
      'category' => 'Wira usaha',
      'title' => 'Usaha Pakaian Muslim Sedang Menjamur',
      'url' => '#'
    ),
    array (
      'id' => '1',
      'image' => 'article-5.jpg',
      'category' => 'Wira usaha',
      'title' => 'Usaha Pakaian Muslim Sedang Menjamur',
      'url' => '#'
    ),
    array (
      'id' => '1',
      'image' => 'article-6.jpg',
      'category' => 'Wira usaha',
      'title' => 'Usaha Pakaian Muslim Sedang Menjamur',
      'url' => '#'
    ),
    array (
      'id' => '1',
      'image' => 'article-1.jpg',
      'category' => 'Wira usaha',
      'title' => 'Usaha Pakaian Muslim Sedang Menjamur',
      'url' => '#'
    ),
    array (
      'id' => '2',
      'image' => 'article-2.jpg',
      'category' => 'Tokoh Inspirasional',
      'title' => 'Bisnis Sukses ala Laudya Cinthya Bella',
      'url' => '#'
    ),
    array (
      'id' => '3',
      'image' => 'article-3.jpg',
      'category' => 'Wira usaha',
      'title' => 'Bisnis Secara Islam',
      'url' => '#'
    ),
    array (
      'id' => '4',
      'image' => 'article-4.jpg',
      'category' => 'Wira usaha',
      'title' => 'Usaha Pakaian Muslim Sedang Menjamur',
      'url' => '#'
    ),
    array (
      'id' => '1',
      'image' => 'article-5.jpg',
      'category' => 'Wira usaha',
      'title' => 'Usaha Pakaian Muslim Sedang Menjamur',
      'url' => '#'
    ),
    array (
      'id' => '1',
      'image' => 'article-6.jpg',
      'category' => 'Wira usaha',
      'title' => 'Usaha Pakaian Muslim Sedang Menjamur',
      'url' => '#'
    )
  );

  $featured_articles = array(
    array (
      'id' => '1',
      'image' => 'article-1.jpg',
      'category' => 'Wira usaha',
      'title' => 'Mengenal Prinsip Perbankan Syariah',
      'url' => '#'
    ),
    array (
      'id' => '2',
      'image' => 'article-2.jpg',
      'category' => 'Wira usaha',
      'title' => 'Mengenal Prinsip Perbankan Syariah',
      'url' => '#'
    ),
    array (
      'id' => '3',
      'image' => 'article-3.jpg',
      'category' => 'Wira usaha',
      'title' => 'Mengenal Prinsip Perbankan Syariah',
      'url' => '#'
    )
  );

  $other_articles = array(
    array(
      'id' => '1',
      'image' => 'article-1.jpg',
      'category' => 'Empowerment',
      'title' => 'Apa itu Cantik?',
      'url' => '#'
    ),
    array(
      'id' => '2',
      'image' => 'article-2.jpg',
      'category' => 'Health',
      'title' => 'Bersepeda Agar Tetap Bugar',
      'url' => '#'
    ),
    array(
      'id' => '3',
      'image' => 'article-3.jpg',
      'category' => 'Parenting',
      'title' => 'Mendidik Anak Agar Mandiri',
      'url' => '#'
    ),
    array(
      'id' => '4',
      'image' => 'article-4.jpg',
      'category' => 'Parenting',
      'title' => 'Mendidik Anak Agar Mandiri',
      'url' => '#'
    ),
    array(
      'id' => '5',
      'image' => 'article-5.jpg',
      'category' => 'Parenting',
      'title' => 'Mendidik Anak Agar Mandiri',
      'url' => '#'
    )
  );

?>
