<?php
  $a = $_GET['article'];

  switch ( $a ) {
    case '1':
      $article_id = '1';
      $article_category = 'Parenting';
      $article_title = 'Pesan Hikmah Luqman Al-Hakim';
      $article_image = null;

      $article_content_1 = '
        <p>Salah satu tujuan pernikahan adalah menghasilkan keturunan yang kelak akan menggantikan kedudukan kita di dunia ini.  Memiliki anak merupakan hal yang didambakan oleh pasangan suami istri karena dianggap sebagai salah satu penyebab kebahagiaan dalam rumah tangga.  Allah menanamkan ke dalam diri manusia rasa kecintaan terhadap anak sebagaimana firman-Nya:</p>

        <p><i>“Dijadikan indah pada (pandangan) manusia kecintaan kepada apa-apa yang diingini, yaitu wanita-wanita, anak-anak, harta yang banyak dari jenis emas, perak, kuda pilihan, binatang-binatang ternak dan sawah ladang.”  (QS Ali Imran [3] : 14)</i></p>

        <p>Membangun manusia bukanlah pekerjaan yang mudah.  Seorang filsuf Cina pernah berkata, <i>“Kalau Anda berpikir untuk setahun ke depan, tanamlah benih.  Kalau untuk sepuluh tahun lagi, tanamlah pohon.  Namun kalau Anda berpikir untuk seratus tahun yang akan datang, maka tanamlah manusia.”</i> Demikian pentingnya membangun manusia karena ia memiliki dampak jangka panjang.  Sehingga dikatakan membangun peradaban artinya juga membangun insan yang berkualitas.</p>

        <p>Ratusan tahun yang lalu, dunia menyaksikan keagungan dan gemilangnya peradaban Islam.  Saat itu Islam diwariskan secara kuat, di dalam dada setiap generasi muda ditanamkan kecintaan akan Islam hingga agama ini berkembang memenuhi jiwanya bahwa hanya Islam-lah satu-satunya kunci kemuliaan dan kebahagiaan.</p>

        <p>Sayangnya, saat ini banyak orangtua yang kehilangan pegangan dalam mendidik anak-anaknya dan kurang memahami cara menanamkan karakter yang baik dalam diri anak mereka.  Al Qur’an banyak memberikan petunjuk bagaimana cara mendidik anak-anak agar termasuk dalam golongan orang-orang yang shaleh sebagaimana terkandung dalam kisah Luqman al-Hakim, ajaran yang penuh hikmah dan tidaklah hikmah ini datang dari Allah kecuali hanya akan membawa kebaikan bagi mereka yang memahami.</p>

        <p>Luqman al-Hakim adalah seorang pria shaleh yang namanya diabadikan dalam Al Qur’an, seorang sosok orangtua teladan yang mendidik anaknya berdasarkan prinsip tauhid dan akhlak mulia.  Meskipun hidup ribuan tahun yang lalu namun ajarannya tetap relevan dijadikan rujukan bagi orangtua masa kini.  Butir-butir penting dari ajarannya dalam membangun karakter anak sebagaimana terkandung dalam Al Qur’an adalah:</p>

        <ol>
          <li>Tidak menyekutukan Allah (QS Luqman [31] : 13).</li>
          <li>Berbakti kepada orangtua (QS Luqman [31] : 14).</li>
          <li>Segala amal akan diperhitungkan (QS Luqman [31] : 16).</li>
          <li>Mendirikan shalat, menyeru kebaikan, mencegah kemungkaran, dan bersabar (QS Luqman [31] :17).</li>
          <li>Tidak sombong dan membanggakan diri (QS Luqman [31] : 18).</li>
          <li>Rendah hati dan melunakkan suara (QS Luqman [31] : 19).</li>
        </ol>

        <p>Bila orangtua menanamkan ajaran-ajaran Luqman kepada anak-anaknya, kelak akan mendapati anak-anaknya termasuk dalam golongan orang-orang yang shaleh.  Ajaran Luqman al-Hakim merupakan ajaran yang penuh hikmah dan tidaklah hikmah ini datang dari Allah kecuali hanya akan membawa kebaikan bagi mereka yang memahami.</p>

        <p><i>/byutique.com</i></p>';

        $article_content_2 = '';
      break;
    case '2':
      $article_id = '2';
      $article_category = 'Renungan';
      $article_title = 'Kebaikan Itu Melembutkan Jiwa';
      $article_image = null;

      $article_content_1 = '
        <p>Kebaikan itu mampu melembutkan hati orang yang melakukannya, juga orang yang menerima kebaikannya.  Perhatikan saja, setelah kita melakukan suatu amalan yang baik, maka hati kita akan merasakan kebahagiaan dan ketenangan.  Begitu pula bila ada orang yang berbuat baik kepada kita, hati kita pun menjadi lembut kepada orang yang telah berbuat baik tersebut, bahkan mendoakan sejuta kebaikan untuknya.</p>

        <p>Maka perhatikanlah betapa dahsyatnya kebaikan itu, ia dapat melembutkan dan menyatukan jiwa-jiwa dalam doa.  Dan Allah menyukai kebaikan.  Bila ingin menjadi seorang wanita yang berhati lembut, perbanyaklah berbuat kebaikan, terutama kepada keluarga dan orang-orang terdekatmu.</p>

        <p>Buah dari pekerjaan seorang hamba Allah yang shaleh adalah kebaikan, yang mampu dirasakan oleh dirinya sendiri maupun orang lain.</p>

        <p>Abdullah bin ‘Abbas radhiallahu’anhuma, seorang ulama yang faqih di kalangan para sahabat Nabi, pernah berkata, <i>"Kebaikan itu menyinari wajah, menyalakan cahaya jiwa, membuka pintu rezeki, menguatkan tubuh, dan menambah cinta dalam hati."</i></p>

        <p><i>/byutique.com</i></p>';

        $article_content_2 = '';
      break;
    case '3':
      $article_id = '3';
      $article_category = 'Empowerment';
      $article_title = 'Apa Itu Cantik?';
      $article_image = null;

      $article_content_1 = '
        <p>Menjadi cantik merupakan impian bagi semua wanita. Begitu banyak wanita berlomba-lomba bahkan tak jarang merogoh uang yang sangat besar hanya untuk mendapatkan label cantik: keindahan fisik pada wajah dan tubuh. Berbagai cara instant pun dilakukan oleh kaum hawa, seperti Botox, hingga operasi plastik sekalipun. Di dalam Kamus Besar Bahasa Indonesia disebutkan bahwa “cantik” memiliki artian rupawan, serasi, serta indah dalam bentuk dan buatannya. Namun, nyatanya deskripsi masyarakat mengenai istilah “cantik” saat ini sudah semakin bias. Secara lumrah masyarakat menganggap bahwa “cantik” memiliki satu konsep dan pandangan yang sama, yaitu memiliki kulit putih dengan tubuh yang langsing. Oleh karena itu tak jarang stigma “menjadi cantik” tersebut seringkali diidentifikasikan oleh masyarakat dengan membenarkan kesan “mahal dan eksklusif”. Hal ini tentu saja menyebabkan banyak wanita Indonesia yang semakin tidak percaya diri bahkan enggan mengakui bahwa mereka cantik.</p>

        <p>Seperti yang dilansir dari lembaga riset independent Brand Marketing Institute (BMI research), menyebutkan bahwa ternyata hanya 1 dari 10 wanita yang menyebut dirinya cantik. Begitu juga data survey global yang dilakukan oleh DOVE sebagai salah satu brand kecantikan menyebutkan bahwa sebanyak 96 % wanita tidak menyebutkan atau mengakui cantik untuk menjelaskan deskripsi tentang dirinya. Fakta ini menjadi sebuah fenomena yang sangat mengejutkan, sebab stereotipe yang digambarkan oleh berbagai ajang kecantikan hingga kehidupan glamor selebritis telah memberikan standar yang begitu tinggi untuk kata “cantik” bagi seorang wanita. Rasa tidak percaya diri hanya karena definisi kata “cantik” yang terlalu berlebihan berdampak terhadap kehidupan pribadi seorang wanita sehingga cara apapun dilakukan untuk memperoleh kepuasan atas definisi “cantik” tersebut.</p>

        <p>Untuk beberapa wanita yang memiliki kemampuan dalam hal finansial, mereka bisa melakukan segala cara untuk memperbaiki bentuk wajah dan tubuh mereka agar bisa terlihat cantik. Lalu apa yang harus dilakukan wanita dengan kemampuan finansial yang terbatas? Tentunya mereka hanya akan terus dirundung rasa ketidakpercayaan diri bahkan memiliki kecemburuan sosial atau persaingan antar sesama wanita.</p>

        <p>Karena itu tak heran jika saat ini berbagai produk kecantikan seolah melakukan “perang dagang” dengan memburu para konsumen wanita yang haus akan definisi “cantik” tersebut. Dengan berbagai macam penawaran serta promosi yang menggiurkan diberikan: Dapat membuat kulit wajah agar telihat lebih putih, atau produk kecantikan yang bisa membuat tubuh lebih langsing dalam kurun waktu yang sangat cepat, terlebih menawarkan harga sangat murah agar laris manis diburu. Para produsen kecantikan menganggap bahwa “cantik” adalah komoditi menarik bagi seluruh kaum hawa dan mereka akan mendapatkan keuntungan dengan mendesak wanita yang ingin cantik, yang terpenting produk itu bisa membuat wajah lebih putih dan tubuh lebih langsing secara cepat tanpa mempedulikan sisi kesehatan atau keamanan dari produk itu sendiri.</p>

        <p>Hal ini tentu membuat kita merasa prihatin. Menurut saya bahwa kecantikan yang sesungguhnya adalah kecantikan dari dalam diri wanita itu sendiri. Kecantikan sejati tentunya memiliki kecantikan hati dan cantik kepribadian yang mencerminkan kecantikan wanita sesungguhnya. Wanita sebaiknya memahami bahwa kecantikan tidak hanya dari penampilan fisik saja, namun setiap wanita diciptakan berbeda dengan kelebihan dan kekurangan masing – masing. Seharusnya kita tidak perlu membandingkan kekurangan kita dengan kelebihan orang lain. Jika kita sibuk melihat kekurangan pada diri sendiri maka tanpa disadari ternyata setiap orang selalu memiliki kelebihan. “Cantik” dapat dimulai dengan mengenali diri sendiri, menerima apa yang sudah diberikan oleh Tuhan dan selanjutnya meningkatkan kelebihan yang kita miliki.</p>

        <p>Sedangkan sebagai muslimah, kita bisa menjadi cantik sesuai dengan tuntunan Islam, yaitu:</p>

        <ol>
          <li>Jadikanlah ghadul bashar (menundukkan pandangan) sebagai celak bagi kedua belah alis mata kalian. Insya Allah, pandangan visual  akan menjadi makin jernih dan bening.</li>
          <li>Oleskanlah lipstik kejujuran dan kebenaran (alhaq) pada ulasan bibir.</li>
          <li>Bedakkanlah raut wajah kalian dengan kosmetik yang berasaskan malu dan keadaban.</li>
          <li>Lumurkanlah sabun istighfar ke sepelusuk anggota badan kalian. Insya Allah, ia bisa mengikis daki kotoran dosa dan kesalahan.</li>
          <li>Rawatkanlah rambut  dengan hijab Islami.</li>
          <li>Sarungkanlah kedua-dua belah tangan  dengan gelang sedekah dan jari-jemari dengan cincin ukhuwwah Islamiyyah.</li>
          <li>Alunkanlah kemerduan dan kesyahduan suara dengan tilawatul Qur’an dan zikrullah.</li>
          <li>Luruskanlah postur tubuh badan kalian dengan ketulusan dalam menunaikan shalat dan pengibadatan kepada Allah semata.</li>
        </ol>

        <p>Namun di sisi lain, wanita juga sebaiknya tidak boleh masa bodoh dengan penampilan. “Cantik” tidak harus mahal ataupun mengubah bentuk tubuh. Kita bisa menjadi “cantik” asalkan kita tetap peduli dengan kondisi tubuh kita, namun tetap merawat diri dan mengenali diri sendiri juga sebaiknya menjadi kewajiban bagi seorang wanita. Perawatan diri khususnya pada fisik pun juga sangat penting dilakukan. Selain membuat kita terlihat cantik secara alami, manfaat lain yang bisa didapatkan adalah tubuh yang menjadi sehat dengan kulit yang lebih bercahaya.</p>

        <p>Nutrisi yang dibutuhkan oleh tubuh pun akan semakin terpenuhi jika kita secara rutin melakukan perawatan secara murah dan aman, seperti treatment dengan menggunakan buah-buahan, minum air putih dan mandi/ sering membersihkan tubuh secara teratur. Jangan biarkan wajah bermasalah, kusam, rambut rontok, beketombe, tubuh lelah dan tidak terawat yang mengakibatkan kita menjadi tidak percaya diri. Melakukan perawatan fisik secara maksimal dapat membuat kepercayaan diri seorang wanita tentunya akan memunculkan kecantikan secara alami dengan sendirinya.</p>

        <p><i>Narasumber: Lindawaty Harras</i></p>';

        $article_content_2 = '';
      break;
    case '4':
      $article_id = '4';
      $article_category = 'Spiritual';
      $article_title = 'Apa yang Perlu Kita Ketahui Tentang Tauhid';
      $article_image = null;

      $article_content_1 = '
        <p>Tauhid dalam bahasa artinya menjadikan sesuatu esa. Yang dimaksud disini adalah mempercayai bahwa Allah itu esa. Sedangkan secara istilah ilmu Tauhid ialah ilmu yang membahas segala kepercayaan-kepercayaan yang diambil dari dalil dalil keyakinan dan hukum-hukum di dalam Islam termasuk hukum mempercayakan Allah itu esa.</p>

        <p>Seandainya ada orang tidak mempercayai keesaan Allah atau mengingkari perkara-perkara yang menjadi dasar ilmu tauhid, maka orang itu dikatagorikan bukan muslim dan digelari kafir. Begitu pula halnya, seandainya seorang muslim menukar kepercayaannya dari mempercayai keesaan Allah, maka kedudukannya juga sama adalah kafir.</p>

        <p>Perkara dasar yang wajib dipercayai dalam ilmu tauhid ialah perkara yang dalilnya atau buktinya cukup terang dan kuat yang terdapat di dalam Al Quran atau Hadis yang shahih. Perkara ini tidak boleh dita’wil atau ditukar maknanya yang asli dengan makna yang lain.</p>

        <p>Tujuan mempelajari ilmu tauhid adalah mengenal Allah dan rasul-Nya dengan dalil dalil yang pasti dan menetapkan sesuatu yang wajib bagi Allah dari sifat sifat yang sempurna dan mensucikan Allah dari tanda tanda kekurangan dan membenarkan semua rasul rasul Nya.</p>

        <p>Adapun perkara yang dibicarakan dalam ilmu tauhid adalah dzat Allah dan dzat para rasul Nya dilihat dari segi apa yang wajib (harus) bagi Allah dan Rasul Nya, apa yang mustahil dan apa yang jaiz (boleh atau tidak boleh)</p>

        <p>Jelasnya, ilmu Tauhid terbagi dalam tiga bagian:</p>

        <ol>
          <li>Wajib</li>
          <li>Mustahil</li>
          <li>Jaiz (Mungkin)</li>
        </ol>

        <p><h3>1. WAJIB</h3></p>

        <p>Wajib dalam ilmu Tauhid berarti menentukan suatu hukum dengan mempergunakan akal bahwa sesuatu itu wajib atau tidak boleh tidak harus demikian hukumnya. Hukum wajib dalam ilmu tauhid ini ditentukan oleh akal tanpa lebih dahulu memerlukan penyelidikan atau menggunakan dalil.</p>

        <p>Contoh yang ringan, uang seribu 1000 rupiah adalah lebih banyak dari 500 rupiah. Artinya akal atau logika kita dapat mengetahui atau menghukum bahwa 1000 rupiah itu lebih banyak dari 500 rupiah. Tidak boleh tidak, harus demikian hukumnya. Contoh lainnya, seorang ayah usianya harus lebih tua dari usia anaknya. Artinya secara akal bahwa si ayah wajib atau harus lebih tua dari si anak</p>

        <p>Ada lagi hukum wajib yang dapat ditentukan bukan dengan akal tapi harus memerlukan penyelidikan yang rapi dan cukup cermat. Contohnya, Bumi itu bulat.  Sebelum akal dapat menentukan bahwa bumi itu bulat, maka wajib atau harus diadakan dahulu penyelidikan dan mencari bukti bahwa bumi itu betul betul bulat. Jadi akal tidak bisa menerima begitu saja tanpa penyelidikan lebih dahulu. Contoh lainnya, sebelum akal menghukum dan menentukan bahwa ”Allah wajib atau harus ada”, maka harus diadakan dahulu penyelidikan yang rapi yang menunjukkan kewujudan atau keberadaan bahwa Allah itu wajib ada. Tentu hal ini perlu dibantu dengan dalil-dalil yang bersumber dari Al Quran.<p>

        <p><h3>2. MUSTAHIL</h3></p>

        <p>Mustahil dalam ilmu tauhid adalah kebalikan dari wajib. Mustahil dalam ilmu tauhid berarti akal mustahil bisa menentukan dan mustahil bisa menghukum bahwa sesuatu itu harus demikian.</p>

        <p>Hukum mustahil dalam ilmu tauhid ini bisa ditentukan oleh akal tanpa lebih dahulu memerlukan penyelidikan atau menggunakan dalil. Contohnya , uang 500 rupiah mustahil lebih banyak dari 1000 rupiah. Artinya akal atau logika kita dapat mengetahui atau menghukum bahwa 500 rupiah itu mustahil akan lebih banyak dari1000 rupiah. Contoh lainnya,  usia seorang anak mustahil lebih tua dari ayahnya. Artinya secara akal bahwa seorang anak mustahil lebih tua dari ayahnya.</p>

        <p>Sebagaimana hukum wajib dalam Ilmu Tauhid, hukum mustahil juga ada yang ditentukan dengan memerlukan penyelidikan yang rapi dan cukup cermat. Contohnya: Mustahil bumi ini berbentuk tiga segi. Jadi sebelum akal dapat menghukum bahwa mustahil bumi ini berbentuk segi tiga, perkara tersebut harus diselidik dengan cermat yang bersenderkan kepada dalil kuat. Contoh lainnya: Mustahil Allah boleh mati. Jadi sebelum akal dapat menghukum bahwa mustahil Allah boleh mati atau dibunuh, maka perkara tersebut hendaklah diselidiki lebih dahulu dengan bersenderkan kepada dalil yang kuat.</p>

        <p><h3>3. JAIZ (MUNGKIN)</h3></p>

        <p>Apa arti Jaiz (mungkin) dalam ilmu Tauhid? Jaiz (mungkin) dalam ilmu tauhid ialah akal kita dapat menentukan atau menghukum bahwa sesuatu benda atau sesuatu dzat itu boleh demikian keadaannya atau boleh juga tidak demikian. Atau dalam arti lainya mungkin demikian atau mungkin tidak. Contohnya: penyakit seseorang itu mungkin bisa sembuh atau mungkin saja tidak bisa sembuh. Seseorang adalah dzat dan sembuh atau tidaknya adalah hukum jaiz (mungkin). Hukum jaiz (Mungkin) disini, tidak memerlukan hujjah atau dalil.</p>

        <p>Contoh lainya: bila langit mendung, mungkin akan turun hujan lebat, mungkin turun hujan rintik rintik, atau mungkin tidak turun hujan sama sekali. Langit mendung dan hujan adalah dzat, sementara lebat, rintik rintik atau tidak turun hujan adalah Hukum jaiz (Mungkin).</p>

        <p>Seperti hukum wajib dan mustahil, hukum jaiz (mungkin) juga kadang kandang memerlukan bukti atau dalil. Contohnya manusia mungkin bisa hidup ratusan tahun tanpa makan dan minum seperti terjadi pada kisah Ashabul Kahfi yang tertera dalam surat al-Kahfi. Kejadian manusia bisa hidup ratusan tahun tanpa makan dan minum mungkin terjadi tapi kita memerlukan dalil yang kuat diambil dari al-Qur’an..</p>

        <p>Contoh lainnya: rumah seseorang dari di satu tempat mungkin bisa berpindah dengan sekejap mata ke tempat yang lain yang jaraknya ribuan kilometer dari tempat asalnya seperti terjadi dalam kisah nabi Sulaiman as telah memindahkan istana Ratu Balqis dari Yaman ke negara Palestina yang jaraknya ribuan kilo meter. Kisah ini sudah barang tentu memerlukan dalil yang diambil dari al-Qu’ran.</p>

        <p><i>Disadur dari tulisan : Hasan Husen Assagaf</i></p>';

        $article_content_2 = '';
      break;
    case '5':
      $article_id = '5';
      $article_category = 'Spiritual';
      $article_title = 'Aisyah Binti Abu Bakar - Istri Kesayangan Nabi Muhammad SAW';
      $article_image = null;

      $article_content_1 = '
        <p>Aisyah binti Abu Bakar adalah istri dari Nabi Muhammad salallahi alaihi wassaalam. ‘Aisyah adalah putri dari Abu Bakar (khalifah pertama), hasil dari pernikahan dengan isteri keduanya yaitu Ummi Ruman yang telah melahirkan Abd al Rahman dan Aisyah. Beliau termasuk ke dalam ummul-mu\'minin (Ibu orang-orang Mukmin). Ia dikutip sebagai sumber dari banyak hadits, dimana kehidupan pribadi Muhammad menjadi topik yang sering dibicarakan.</p>

        <p>Aisyah binti Abu Bakar adalah satu-satunya istri Nabi Muhammad yang saat dinikah oleh Nabi Muhammad berstatus gadis. Sedangkan istri-istri Nabi Muhammad yang lain umumnya adalah janda.</p>

        <p>Nama dan Nasab - Beliau adalah ‘Aisyah binti Abu Bakar ash-Shiddiq bin Abu Quhafah bin ‘Amir bin ‘Amr bin Ka’ab bin Sa’ad bin Taim bin Murrah bin Ka’ab bin Lu’ay. Ibunda beliau bernama Ummu Rumman binti ‘Umair bin ‘Amir bin Dahman bin Harist bin Ghanam bin Malik bin Kinanah.</p>

        <p><h3>Pernikahan dengan Nabi Muhammad SAW</h3></p>

        <p>‘Aisyah .ra terlahir empat atau lima tahun setelah diutusnya Rasulullah salallahi alaihi wassaalam. Ayah Aisyah, Abu Bakar merasa Aisyah sudah cukup umur untuk menikah, karena hal itu, Aisyah akan dinikahkan dengan Jubayr bin Mut\'im, tetapi pernikahan tersebut tidak terjadi disebabkan Ayah Jubair, Mut‘im bin ‘Adi menolak aisyah dikarenakan Abu Bakar telah masuk Islam pada saat itu. Istri Mut\'im bin Adi mengatakan tidak mau keluarganya mempunyai hubungan dengan para muslim, yang dapat menyebabkan Jubair menjadi seorang Muslim.</p>

        <p>Menurut Tabari (juga menurut Hisham ibn `Urwah, Ibn Hunbal and Ibn Sad), Aisyah dipinang pada usia 7 tahun dan mulai berumah tangga pada usia 9 tahun, dimana Aisyah menjadi istri ketiga Muhammad setelah Khadijah dan Saudah binti Zam\'ah. Tetapi terdapat berbagai silang pendapat mengenai pada umur berapa sebenarnya Muhammad menikahi Aisyah? Sebagian besar referensi (termasuk sahih Bukhari dan sahih Muslim) menyatakan bahwa upacara perkawinan tersebut terjadi di usia enam tahun, dan Aisyah diantarkan memasuki rumah tangga Muhammad sejak umur sembilan tahun.</p>

        Sedangkan menurut penelitian yang dilakukan Ghulam Nabi Muslim Sahib, dengan berdasarkan referensi dari Kitab Ahmal fi Asma’ al-Rijjal karangan al-Khatib al-Tibrizi dimana dalam kitab tersebut disebutkan Setidaknya Aisyah berumur 19 tahun saat menikah dengan Nabi.</p>

        <p><h3>Keutamaan Aisyah ra</h3></p>

        <p>Pribadi yang Haus Ilmu - Selama Sembilan tahun  hidup dengan Rasulullah saw. Beliau dikenal sebagai pribadi yang haus akan ilmu pengetahuan. Ketekunan dalam belajar menghantarkan beliau sebagai perempuan yang banyak menguasai berbagai bidang ilmu. Diantaranya adalah ilmu al-qur’an, hadist, fiqih, bahasa arab dan syair. Keilmuan Aisyah tidak diragukan lagi karena beliau adalah orang terdekat Rasulullah yang sering mengikuti pribadi Rasulullah.  Banyak wahyu yang turun dari Allah disaksikan langsung oleh Aisyah ra.</p>

        <p>“Aku pernah melihat wahyu turun kepada Rasulullah pada suatu hari yang sangat dingin sehingga beliau tidak sadarkan diri, sementara keringat bercucuran dari dahi beliau.“ (HR. Bukhari).</p>

        <p>Periwayat Hadist - Aisyah juga dikenal sebagai perempuan yang banyak menghapalkan hadist-hadist Rasulullah. Sehingga beliau mendapat gelar Al-mukatsirin (orang yang paling banyak meriwayatkan hadist). Ada sebanyak 2210 hadist yang diriwayatkan oleh Aisyah ra. Diantaranya terdapat 297 hadist  dalam kitab shahihain dan sebanyak 174 hadist yang mencapai derajat muttafaq ‘alaih. Bahkan para ahli hadist menempatkan beliau pada posisi kelima penghafal hadist setelah Abu Hurairah, Ibnu Umar, Anas bin Malik, dan Ibnu Abbas.</p>

        <p>Kecerdasan dan keluasan ilmu yang dimiliki  Aisyah ra sudah tidak diragukan lagi. Bahkan beliau dijadikan tempat bertanya para kaum wanita dan para sahabat tentang permasalahan hukum agama, maupun kehidupan pribadi kaum muslimin secara umum.</p>

        <p>Hisyam bin Urwah meriwayatkan hadis dari ayahnya. Dia mengatakan: “Sungguh aku telah banyak belajar dari ‘Aisyah. Belum pernah aku melihat seorang pun yang lebih pandai daripada ‘Aisyah tentang ayat-ayat Al-Qur’an yang sudah diturunkan, hukum fardhu dan sunnah, syair, permasalahan yang ditanyakan kepadanya, hari-hari yang digunakan di tanah Arab, nasab, hukum, serta pengobatan."</p>

        <p>Pribadi yang Tegas dalam Menegakkan Hukum Allah - Aisyah juga dikenal sebagai pribadi yang tegas dalam mengambil sikap. Hal ini terlihat dalam penegakan hukum Allah, Aisyah langsung menegur perempuan-perempuan muslim yang melanggar hukum Allah.</p>

        <p>Suatu ketika dia mendengar bahwa kaum wanita dari Hamash di Syam mandi di tempat pemandian umum. Aisyah mendatangi mereka dan berkata, “Aku mendengar Rasulullah Shallallahu alaihi wassalam. bersabda, ‘Perempuan yang menanggalkan pakaiannya di rumah selain rumah suaminya maka dia telah membuka tabir penutup antara dia dengan Tuhannya.“ (HR. Ahmad, Abu Daud, dan Ibnu Majah)</p>

        <p>Aisyah pun pernah menyaksikan adanya perubahan pada pakaian yang dikenakan wanita-wanita Islam setelah Rasulullah wafat. Aisyah menentang perubahan tersebut seraya berkata, “Seandainya Rasulullah melihat apa yang terjadi pada wanita (masa kini), niscaya beliau akan melarang mereka memasuki masjid sebagaimana wanita Israel dilarang memasuki tempat ibadah mereka.”</p>

        Di dalam Thabaqat Ibnu Saad mengatakan bahwa Hafshah binti Abdirrahman menemui Ummul-Mukminin Aisyah. Ketika itu Hafsyah mengenakan kerudung tipis. Secepat kilat Aisyah menarik kerudung tersebut dan menggantinya dengan kerudung yang tebal.</p>

        Pribadi yang Dermawan - Dalam hidupnya Aisyah ra juga dikenal sebagai pribadi yang dermawan. Dalam sebuah kisah diceritakan bahwa Aisyah ra pernah menerima uang sebanyak 100.000 dirham. Kemudian beliau meminta para pembantunya untuk membagi-bagikan uang tersebut kepada fakir miskin tanpa menyisakan satu dirhampun untuk beliau. Padahal saat itu beliau sedang berpuasa.</p>

        <p>Harta duniawi tidak menyilaukan Aisyah ra. Meskipun pada saat itu kelimpahan kekayaan berpihak kepada kaum muslimin. Aisyah ra tetap hidup dalam kesederhanaan sebagaimana yang dicontohkan oleh Rasulullah saw.</p>

        <p>Setelah Rasulullah meninggal dunia, Aisyah ra menghabiskan hidupnya untuk perkembangan dan kemajuan Islam. Rumah beliau tak pernah sepi dari pengunjung untuk bertanya berbagai permasalahan syar’iat . Sampai-sampai Khalifah Umar bin khatab dan Usman bin Affan mengangkat beliau menjadi penasehat. Hal ini merupakan wujud penghormatan  Umar dan Ustman terhadap kemuliaan Ilmu yang dimiliki oleh Aisyah ra.</p>

        <p><h3>Wafatnya ‘Aisyah .ra</h3></p>

        <p>‘Aisyah .ra meninggal pada malam selasa, tanggal 17 Ramadhan setelah shalat witir, pada tahun 58 Hijriyah. Yang demikian itu menurut pendapat mayoritas ulama. Ada juga yang berpendapat bahwa beliau wafat pada tahun 57 H, dalam usia 63 tahun dan sekian bulan. Para sahabat Anshar berdatangan pada saat itu, bahkan tidak pernah ditemukan satu hari pun yang lebih banyak orang-orang berkumpul padanya daripada hari itu, sampai-sampai penduduk sekitar Madinah turut berdatangan.</p>

        <p>‘Aisyah .ra dikuburkan di Pekuburan Baqi’. Shalat jenazahnya diimami oleh Abu Hurairah dan Marwan bin Hakam yang saat itu adalah Gubernur Madinah.</p>

        Sosok Aisyah ra merupakan teladan yang tepat bagi muslimah tanpa perlu menggembar-gemborkan masalah emansipasi yang terjadi saat ini. Keberadaan Aisyah sudah membuktikan bahwa perempuan juga diberikan posisi yang layak di zaman Rasulullah saw dan para shahabat.</p>

        <p><i>Referensi:</i></p>

        <ul>
          <li><i>Biografi Ummul Mu’minin Aisyah radhiyallahu ‘anha</i></li>
          <li><i>Aisyah binti Abu Bakar</i></li>
          <li><i>kisah Siti Aisyah Binti Abu Bakar R.A.(isteri kesayangan Nabi Muhammad S.A.W)</i></li>
          <li><i>Aisyah binti Abu Bakar As-Shidiq: Perempuan Teladan Sepanjang Masa</i></li>
          <li><i>Aisyah</i></li>
        </ul>';

        $article_content_2 = '';
      break;
    case '6':
      $article_id = '6';
      $article_category = 'Spiritual';
      $article_title = 'Khadijah Binti Khuwalid Cinta Sejati Nya Rasulullah SAW';
      $article_image = null;

      $article_content_1 = '
        <p>Khadijah binti Khuwailid merupakan isteri pertama Nabi Muhammad. Beliau adalah Ummul Mukminin istri Rasulullah yang pertama, wanita pertama yang mernpercayai risalah Rasulullah, dan wanita pertama yang melahirkan putra-putri Rasulullah. Dia merelakan harta benda yang dimilikinya untuk kepentingan jihad di jalan Allah. Dialah orang pertama yang mendapat kabar gembira bahwa dirinya adalah ahli surga.</p>

        <p>Nama lengkapnya adalah Khadijah binti Khuwailid bin Asad bin Abdul Uzza bin Qushai. Khadijah al-Kubra, anak perempuan dari Khuwailid bin Asad dan Fatimah binti Za\'idah, berasal dari kabilah Bani Asad dari sukuQuraisy. Ia merupakan wanita as-Sabiqun al-Awwalun.</p>

        <p>Khadijah binti Khuwailid adalah sebaik-baik wanita ahli surga. Ini sebagaimana sabda Rasulullah, “Sebaik-baik wanita ahli surga adalah Maryam binti Imran dan Khadijah binti Khuwailid.”</p>

        <p>Khadijah lahir sekitar tahun 555/565/570, beliau berasal dari golongan pembesar Mekkah. Menikah dengan Nabi Muhammad, ketika berumur 40 tahun, manakala Nabi Muhammad berumur 25 tahun. Ada yang mengatakan usianya saat itu tidak sampai 40 tahun, hanya sedikit lebih tua dari Nabi Muhammad. Khadijah merupakan wanita kaya dan terkenal. Khadijah bisa hidup mewah dengan hartanya sendiri. Meskipun memiliki kekayaan melimpah, Khadijah merasa kesepian hidup menyendiri tanpa suami, karena suami pertama dan keduanya telah meninggal. Suami pertama Khadijah adalah Abu Halah at-Tamimi, yang wafat dengan meninggalkan kekayaan yang banyak, juga jaringan perniagaan yang luas dan berkembang. Pernikahan kedua Khadijah adalah dengan Atiq bin Aidz bin Makhzum, yang juga wafat dengan meninggalkan harta dan perniagaan. Dengan demikian, Khadijah menjadi orang terkaya di kalangan suku Quraisy. Namun, beberapa sumber menyangkal bahwa Khadijah pernah menikah sebelum bertemu Nabi Muhammad SAW.</p>

        <p><h3>Mengurus harta</h3></p>

        <p>Sayyidah Khadijah dikenal dengan julukan wanita suci sejak perkawinannya dengan Abu Halah dan Atiq bin Aidz karena keutamaan ãkhlak dan sifat terpujinya. Karena itu, kalangan Quraisy memberikan penghargaan dan penghormatan yang tinggi kepadanya.</p>

        <p>Dengan kekayaannya yang berlimpah tentu Khadijah tidak dapat bekerja sendiri, maka ia mengangkat beberapa pegawai untuk membawa dagangannya ke Yaman pada musim dingin dan ke Syam pada musim panas. Diantara para pegawai tersebut terdapat seseorang yang paling dipercaya dan dikenal dengan nama Maisarah. Dia dikenal sebagai pemuda yang ikhlas dan berani, sehingga Khadijah pun berani melimpahkan tanggung jawab untuk pengangkatan pegawai baru yang akan mengiring dan menyiapkan kafilah, menentukan harga, dan memilih barang dagangan. Sebenarnya itu adalah pekerjaan berat, namun penugasan kepada Maisarah tidaklah sia-sia.</p>

        <p><h3>Mengenal Muhammad bin Abdullah</h3></p>

        <p>Muhammad bin Abdullah adalah seorang pemuda dari Kaum Quraisy yang wara, takwa, dan jujur. Sejak usia lima belas tahun, Muhammad bin Abdullah telah diajak oleh Maisarah untuk menyertainya berdagang.</p>

        <p>Selama berniaga dengan Muhammad bin Abdullah, Maisarah sering mendapat keuntungan yang sangat besar akibat dari kejujuran Muhammad dalam berdagang. Selain itu selama berniaga, dia melihat gulungan awan tebal yang senantiasa mengiringi Muhammad yang seolah-olah melindungi beliau dari sengatan matahari. Dia pun mendengar seorang rahib yang bernama Buhairah, yang mengatakan bahwa Muhammad adalah laki-laki yang akan menjadi nabi yang ditunggu-tunggu oleh orang Arab sebagaimana telah tertulis di dalam Taurat dan Injil. Semua hal tersebut diceritakan Maisarah kepada Khadijah.</p>

        <p>Mendengar cerita dari Maisarah,  menimbulkan kecenderungan perasaan Khadijah terhadap Muhammad, sehingga dia menemui anak pamannya, Waraqah bin Naufal. Waraqah mengatakan bahwa akan muncul nabi besar yang dinanti-nantikan manusia dan akan mengeluarkan manusia dari kegelapan menuju cahaya Allah. Penuturan Waraqah itu menjadikan niat dan kecenderungan Khadijah terhadap Muhammad semakin bertambah, sehingga dia ingin menikah dengan Muhammad. Setelah itu dia mengutus Nafisah, saudara perempuan Ya’la bin Umayyah untuk meneliti lebih jauh tentang Muhammad, sehingga akhirnya Muhammad diminta menikahi dirinya.</p>

        <p>Ketika itu Khadijah berusia empat puluh tahun, namun dia adalah wanita dari golongan keluarga terhormat dan kaya raya, sehingga banyak pemuda Quraisy yang ingin menikahinya. Muhammad pun menyetujui permohonan Khadijah tersebut. Maka, dengan salah seorang pamannya, Muhammad pergi menemui paman Khadijah yang bernama Amru bin As’ad untuk meminang Khadijah.</p>

        <p>Ketika itu, usia Muhammad berusia dua puluh lima tahun, sementara Khadijah empat puluh tahun. Walaupun usia mereka terpaut sangat jauh dan harta kekayaan mereka pun tidak sepadan, pernikahan mereka bukanlah pernikahan yang aneh, karena Allah Subhanahu wa ta’ala telah memberikan keberkahan dan kemuliaan kepada mereka.</p>

        <p><h3>Keturunan Rasulullah Shallallahu alaihi wassalam</h3></p>

        <p>Setelah menikah dengan Nabi Muhammad SAW, Khadijah melahirkan dua orang anak laki-laki, yaitu Qasim dan Abdullah serta empat orang anak perempuan, yaitu Zainab, Ruqayah, Ummu Kultsum dan Fatimah. Seluruh putra dan putrinya lahir sebelum masa kenabian, kecuali Abdullah. Karena itulah, Abdullah kemudian dijuluki ath-Thayyib (yang balk) dan ath-Thahir (yang suci).</p>

        <p>Zainab banyak rnenyerupai ibunya. Setelah besar, Zainab dinikahkan dengan anak bibinya, Abul Ash ibnur Rabi’. Pernikahan Zainab ini merupakan peristiwa pertama Rasulullah rnenikahkan putrinya, dan yang terakhir beliau menikahkan Ummu Kultsum dan Ruqayah dengan dua putra Abu Lahab, yaitu Atabah dan Utaibah. Ketika Nabi Shallallahu alaihi wassalam diutus menjadi Rasul, Fathimah az-Zahra, putri bungsu beliau rnasih kecil.</p>

        <p>Selain mereka ada juga Zaid bin Haritsah yang sering disebut putra Muhammad. Semula, Zaid dibeli oleh Khadijah dari pasar Mekah yang kemudian dijadikan budaknya. Ketika Khadijah menikah dengan Muhammad, Khadijah memberikan Zaid kepada Muhammad sebagai hadiah. Rasulullah sangat mencintai Zaid karena dia memiliki sifat-sifat yang terpuji. Zaid pun sangat mencintai Rasulullah. Akan tetapi di tempat lain, ayah kandung Zaid selalu mencari anaknya dan akhirnya dia mendapat kabar bahwa Zaid berada di tempat Muhammad dan Khadijah. Dia mendatangi Rasulullah Shallallahu alaihi wassalam untuk memohon agar beliau mengembalikan Zaid kepadanya walaupun dia harus membayar mahal. Rasulullah Shallallahu alaihi wassalam memberikan kebebasan penuh kepada Zaid untuk memilih antara tetáp tinggal bersamanya dan ikut bersama ayahnya. Zaid tetap memilih hidup bersama Rasulullah, sehingga dari sinilah kita dapat mengetahui sifat mulia Zaid.</p>

        <p>Agar pada kemudian hari nanti tidak menjadi masalah yang akan memberatkan ayahnya, Rasulullah Shallallahu alaihi wassalam dan Zaid bin Haritsah menuju halaman Ka’bah untuk mengumumkan kebebasan dan pengangkatan Zaid sebagai anak. Setelah itu, ayahnya merelakan anaknya dan merasa tenang. Dari situlah mengapa banyak yang menjuluki Zaid dengan sebutan Zaid bin Muhammad. Akan tetapi, hukum pengangkatan anak itu gugur setelah turun ayat yang membatalkannya, karena hal itu merupakan adat jahiliah, sebagaimana firman Allah berikut ini:</p>

        <p>” … jika kamu mengetahui bapak-bapak mereka, maka (panggillah mereka sebagai) saudara-saudaramu seagama dan maula-maulamu … ” (QS. At-Taubah:5)</p>

        <p><h3>Masa Kenabian Muhammad Shallallahu alaihi wassalam</h3></p>

        <p>Muhammad bin Abdullah hidup berumah tangga dengan Khadijah binti Khuwailid dengan tenterarn di bawah naungan akhlak mulia dan jiwa suci sang suami. Ketika itu, Rasulullah Shallallahu alaihi wassalam menjadi tempat mengadu orang-orang Quraisy dalam menyelesaikan perselisihan dan pertentangan yang terjadi di antara mereka. Hal itu menunjukkan betapa tinggi kedudukan Rasulullah di hadapan mereka pada masa prakenabian. Beliau menyendiri di Gua Hira, menghambakan diri kepada Allah yang Maha Esa, sesuai dengan ajaran Nabi Ibrahim a.s.</p>

        <p>Khadijah sangat ikhlas dengan segala sesuatu yang dilakukan suaminya dan tidak khawatir selama ditinggal suaminya. Bahkan dia menjenguk serta menyiapkan makanan dan minuman selama beliau di dalam gua, karena dia yakin bahwa apa pun yang dilakukan suaminya merupakan masalah penting yang akan mengubah dunia. Ketika itu, Nabi Muhammad berusia empat puluh tahun.

        <p>Saat Rasulullah menerima wahyu pertama di gua hira, maka Khadijah memberikan ketenteraman kepada Rasulullah dengan segala kelembutan dan kasih sayang sehingga beliau merasa tenteram dan aman. Beliau ridak langsung menceritakan kejadian yang menimpa dirinya kepada Khadijah karena khawatir Khadijah menganggapnya sebagai ilusi atau khayalan beliau belaka. untuk lengkapnya silahkan baca di "Biografi Nabi Muhammad S.A.W Nabi dan Rosul Terakhir Panutan Bagi Seluruh Umat Manusia"</p>

        <p><h3>Awal Masa Jihad di Jalan Allah</h3></p>

        <p>Khadijah meyakini seruan suaminya dan menganut agarna yang dibawanya sebelum diumumkan kepada rnasyarakat. Itulah langkah awal Khadijah dalam menyertai suaminya berjihad di jalan Allah dan turut menanggung pahit getirnya gangguan dalam menyebarkan agama Allah.</p>

        <p>Beberapa waktu kemudian Jibril kembali mendatangi Nabi Muhammad Shallallahu alaihi wassalam. untuk membawa wahyu kedua dari Allah:</p>

        <p>“Hai orang yang berkemul (berselimut), bangunlah, lalu berilah peringatan dan Tuhanmu agungkanlah dan pakaianmu bersihkanlah, dan perbuatan dosa (menyembah berhala) tinggalkanlah, dan janganlah kamu memberi (dengan maksud) memperoleb (balasan) yang lebih banyak. Dan untuk (memenuhi perintah) Tuhanmu, bersabarlah” (QS. Al-Muddatstir:1-7)</p>

        <p>Ayat di atas merupakan perintah bagi Rasulullah untuk mulai berdakwah kepada kalangan kerabat dekat dan ahlulbait beliau. Khadijah adalah orang pertama yang menyatakan beriman pada risalah Rasulullah Muhammad dan menyatakan kesediaannya menjadi pembela setia Nabi. Kemudian menyusul Sahabat-sahabat Nabi lainnya.</p>

        <p><h3>Masa Berdakwah Terang-terangan</h3></p>

        <p>Setelah berdakwah secara sembunyi- sembunyi, turunlah perintah Allah kepada Rasulullah untuk memulai dakwah secara terang-terangan. Seruan beliau sangat aneh terdengar di telinga orang-orang Quraisy. Rasulullah Muhammad memanggil manusia untuk beribadah kepada Tuhan yang satu, bukan Laata, Uzza, Hubal, Manat, serta tuhan-tuhan lain yang mernenuhi pelataran Ka’bah. Tentu saja mereka menolak, mencaci maki, bahkan tidak segan-segan menyiksa Rasulullah. Setiap jalan yang beliau lalui ditaburi kotoran hewan dan duri.</p>

        <p>Khadijah tampil mendampingi Rasulullah dengan penuh kasih sayang, cinta, dan kelembutan. Wajahnya senantiasa membiaskan keceriaan, dan bibirnya meluncur kata-kata jujur. Setiap kegundahan yang Rasulullah lontarkan atas perlakuan orang-orang Quraisy selalu didengarkan oleh Khadijah dengan penuh perhatian untuk kemudian dia memotivasi dan rnenguatkan hati Nabi Muhammad Shallallahu alaihi wassalam.</p>

        <p>Khadijah adalah tempat berlindung bagi Rasulullah. Dari Khadijah, beliau memperoleh keteduhan hati dan keceriaan wajah istrinya yang senantiasa menambah semangat dan kesabaran untuk terus berjuang menyebarluaskan agama Allah ke seluruh penjuru. Khadijah pun tidak memperhitungkan harta bendanya yang habis digunakan dalam perjuangan ini. Sementara itu, Abu Thalib, parnan Rasulullah, menjadi benteng pertahanan beliau dan menjaga beliau dari siksaan orang-orang Quraisy, sebab Abu Thalib adalah figur yang sangat disegani dan diperhitungkan oleh kaum Quraisy.</p>

        <p><h3>Wafatnya Khadijah</h3></p>

        <p>Setelah berbagai upaya gagal dilakukan untuk menghentikan dakwah Rasulullah Shallallahu alaihi wassalam, baik itu berupa rayuan, intimidasi, dan penyiksaan, kaum Quraisy memutuskan untuk memboikot dan mengepung kaum muslimin dan menulis deklarasi yang kemudian digantung di pintu Ka’bah agar orang-orang Quraisy memboikot kaum muslimin, termasuk Rasulullah, istrinya, dan juga pamannya. Mereka terisolasi di pinggiran kota Mekah dan diboikot oleh kaum Quraisy dalam bentuk embargo atas transportasi, komunikasi, dan keperluan sehari-hari lainnya.</p>

        <p>Beberapa hari setelah pemboikotan, Abu Thalib jatuh sakit, dan semua orang meyakini bahwa sakit kali mi merupakan akhir dan hidupnva. Abu Thalib meninggal pada tahun itu pula, maka tahun itu disebut sebagai ‘Aamul Huzni (tahun kesedihan) dalam kehidupan Rasulullah Shallallahu alaihi wassalam.</p>

        <p>Pada tahun yang sama, Sayyidah Khadijah sakit keras akibat beberapa tahun menderita kelaparan dan kehausan karena pemboikotan itu. Semakin hari, kondisi badannya semakin menurun, sehingga Rasulullah Shallallahu alaihi wassalam semakin sedih. Dalam sakit yang tidak terlalu lama, dalam usia enam puluh lima tahun Khadijah. Khadijah dikuburkan di dataran tinggi Mekah, yang dikenal dengan sebutan al-Hajun. Rasulullah Shallallahu alaihi wassalam sendiri yang mengurus jenazah istrinya dan kalimat terakhir yang beliau ucapkan ketika melepas kepergiannya adalah: “Sebaik-baik wanita penghuni surga adalah Maryam binti Imran dan Khadijah binti Khuwailid.”</p>

        <p><i>Sumber:<br>
        Buku Dzaujatur-Rasulullah, karya Amru Yusuf, Penerbit Darus-Sa’abu, Riyadh<br>
        Ahlul Hadiits</i></p>';

        $article_content_2 = '';
      break;
    case '7':
      $article_id = '7';
      $article_category = 'Kesehatan';
      $article_title = 'Bagaimana Konsep Kesehatan dalam Islam';
      $article_image = null;

      $article_content_1 = '
        <p>Islam sebagai agama yang sempurna dan lengkap. Telah menetapkan prinsip-prinsip dalam penjagaan keseimbangan tubuh manusia. Diantara cara Islam menjaga kesehatan dengan menjaga kebersihan dan melaksanakan syariat wudlu dan mandi secara rutin bagi setiap muslim.</p>

        <p>Sehat adalah kondisi fisik di mana semua fungsi berada dalam keadaan sehat. Menjadi sembuh sesudah sakit adalah anugerah terbaik dari Allah kepada manusia. Adalah tak mungkin untuk bertindak benar dan memberi perhatian yang layak kepada ketaatan kepada Tuhan jika tubuh tidak sehat.</p>

        <p>Tidak ada sesuatu yang begitu berharga seperti kesehatan. Karenanya, hamba Allah hendaklah bersyukur atas kesehatan yang dimiltkinya dan tidak bersikap kufur. Nabi saw. bersabda, “Ada dua anugerah yang karenanya banyak manusia tertipu, yaitu kesehatan yang baik dan waktu luang.” (HR. Bukhari)</p>

        <p>Abu Darda berkata, “Ya Rasulullah, jika saya sembuh dari sakit saya dan bersyukur karenanya, apakah itu lebih baik daripada saya sakit dan menanggungnya dengan sabar?” Nabi saw menjawab, “Sesungguhnya Rasul mencintai kesehatan sama seperti engkau juga menyenanginya.”</p>

        <p>Diriwayatkan oleh  at-Tirmidzi bahwa Rasulullah saw bersabda: ‘Barangsiapa bangun di pagi hari dengan badan schat dan jiwa sehat pula, dan rezekinya dijamin, maka dia seperti orang yang memiliki dunia seluruhnya.”</p>

        <p><h3>Anjuran Menjaga Kesehatan</h3></p>

        <p>Sudah menjadi semacam kesepakatan, bahwa menjaga agar tetap sehat dan tidak terkena penyakit adalah lebih baik daripada mengobati, untuk itu sejak dini diupayakan agar orang tetap sehat. Menjaga kesehatan sewaktu sehat adalah lebih baik daripada meminum obat saat sakit. Dalam kaidah ushuliyyat dinyatakan:</p>

        <p>Dari Ibn ‘Abbas, ia berkata, aku pernah datang menghadap Rasulullah SAW, saya bertanya: Ya Rasulullah ajarkan kepadaku sesuatu doa yang akan akan baca dalam doaku, Nabi menjawab: Mintalah kepada Allah ampunan dan kesehatan, kemudian aku menghadap lagipada kesempatan yang lain saya bertanya: Ya Rasulullah ajarkan kepadaku sesuatu doa yang akan akan baca dalam doaku. Nabi menjawab: “Wahai Abbas, wahai paman Rasulullah saw mintalah kesehatan kepada Allah, di dunia dan akhirat.” (HR Ahmad, al-Tumudzi, dan al-Bazzar)</p>

        <p>Berbagai upaya yang mesti dilakukan agar orang tetap sehat menurut para pakar kesehatan, antara lain, dengan mengonsumsi gizi yang yang cukup, olahraga cukup, jiwa tenang, serta menjauhkan diri dari berbagai pengaruh yang dapat menjadikannya terjangkit penyakit. Hal-hal tersebut semuanya ada dalam ajaran Islam, bersumber dari hadits-hadits shahih maupun ayat al-Quran.</p>

        <p><h3>Kesehatan Jasmani</h3></p>

        <p>Ajaran Islam sangat menekankan kesehatan jasmani. Agar tetap sehat, hal yang perlu diperhatikan dan dijaga, menurut sementara ulama, disebutkan, ada sepuluh hal, yaitu: dalam hal makan, minum, gerak, diam, tidur, terjaga, hubungan seksual, keinginan-keinginan nafsu, keadaan kejiwaan, dan mengatur anggota badan.</p>

        <p><h3>Pertama; Mengatur Pola Makan dan Minum</h3></p>

        <p>Dalam ilmu kesehatan atau gizi disebutkan, makanan adalah unsur terpenting untuk menjaga kesehatan. Kalangan ahli kedokteran Islam menyebutkan, makan yang halalan dan thayyiban. Al-Quran berpesan agar manusia memperhatikan yang dimakannya, seperti ditegaskan dalam ayat: “maka hendaklah manusia itu memperhatikan makanannya”.(QS. ‘Abasa 80 : 24 )</p>

        <p>Dalam 27 kali pembicaraan tentang perintah makan, al-Quran selalu menekankan dua sifat, yang halal dan thayyib, di antaranya dalam (Q., s. al-Baqarat (2)1168; al-Maidat (s):88; al-Anfal (8):&9; al-Nahl (16) : 1 14), </p>

        <p><h3>Kedua; Keseimbangan Beraktivitas dan Istirahat</h3></p>

        <p>Perhatian Islam terhadap masalah kesehatan dimulai sejak bayi, di mana Islam menekankan bagi ibu agar menyusui anaknya, di samping merupakan fitrah juga mengandung nilai kesehatan. Banyak ayat dalam al-Quran menganjurkan hal tersebut.</p>

        <p>Al-Quran melarang melakukan sesuatu yang dapat merusak badan. Para pakar di bidang medis memberikan contoh seperti merokok. Alasannya, termasuk dalam larangan membinasakan diri dan mubadzir dan akibatyang ditimbulkan, bau, mengganggu orang lain dan lingkungan.</p>

        <p>Islam juga memberikan hak badan, sesuai dengan fungsi dan daya tahannya, sesuai anjuran Nabi: Bahwa badanmu mempunyai hak</p>

        <p>Islam menekankan keteraturan mengatur ritme hidup dengan cara tidur cukup, istirahat cukup, di samping hak-haknya kepada Tuhan melalui ibadah. Islam memberi tuntunan agar mengatur waktu untuk istirahat bagi jasmani. Keteraturan tidur dan berjaga diatur secara proporsional, masing-masing anggota tubuh memiliki hak yang mesti dipenuhi.</p>

        <p>Di sisi lain, Islam melarang membebani badan melebihi batas kemampuannya, seperti melakukan begadang sepanjang malam, melaparkan perut berkepanjangan sekalipun maksudnya untuk beribadah, seperti tampak pada tekad sekelompok Sahabat Nabi yang ingin terus menerus shalat malam dengan tidak tidur, sebagian hendak berpuasa terus menerus sepanjang tahun, dan yang lain tidak mau ‘menggauli’ istrinya, sebagaimana disebutkan dalam hadits:</p>

        <p>“Nabi pernah berkata kepadaku: Hai hamba Allah, bukankah aku memberitakan bahwa kamu puasa di sz’am? hari dan qiyamul laildimalam hari, maka aku katakan, benarya Rasulullah, Nabi menjawab: Jangan lalukan itu, berpuasa dan berbukalah, bangun malam dan tidurlah, sebab, pada badanmu ada hak dan pada lambungmujuga ada hak” (HR Bukhari dan Muslim).</p>

        <p><h3>Ketiga; Olahraga sebagai Upaya Menjaga Kesehatan</h3></p>

        <p>Aktivitas terpenting untuk menjaga kesehatan dalam ilmu kesehatan adalah melalui kegiatan berolahraga. Kata olahraga atau sport (bahasa Inggris) berasal dari bahasa Latin Disportorea atau deportore, dalam bahasa Itali disebut ‘deporte’ yang berarti penyenangan, pemeliharaan atau menghibur untuk bergembira. Olahraga atau sport dirumuskan sebagai kesibukan manusia untuk menggembirakan diri sambil memelihara jasmaniah.</p>

        <p>Tujuan utama olahraga adalah untuk mempertinggi kesehatan yang positif, daya tahan, tenaga otot, keseimbangan emosional, efisiensi dari fungsi-rungsi alat tubuh, dan daya ekspresif serta daya kreatif. Dengan melakukan olahraga secara bertahap, teratur, dan cukup akan meningkatkan dan memperbaiki kesegaran jasmani, menguatkan dan menyehatkan tubuh. Dengan kesegaran jasmani seseorang akan mampu beraktivitas dengan baik.</p>

        <p>Dalam pandangan ulama fikih, olahraga (Bahasa Arab: al-Riyadhat) termasuk bidang ijtihadiyat. Secara umum hokum melakukannya adalah mubah, bahkan bisa bernilai ibadah, jika diniati ibadah atau agar mampu melakukannya melakukan ibadah dengan sempurna dan pelaksanaannyatidakbertentangan dengan norma Islami.</p>

        <p>Sumber ajaran Islam tidak mengatur secara rinci masalah yang berhubungan dengan berolahraga, karena termasuk masalah ‘duniawi’ atau ijtihadiyat, maka bentuk, teknik, dan peraturannya diserahkan sepenuhnya kepada manusia atau ahlinya. Islam hanya memberikan prinsip dan landasan umum yang harus dipatuhi dalam kegiatan berolahraga.</p>

        <p>Nash al-Quran yang dijadikan sebagai pedoman perlunya berolahraga, dalam konteks perintah jihad agar mempersiapkan kekuatan untuk menghadapi kemungkinan serangan musuh, yaitu ayat:</p>

        <p>“Dan siapkanlah untuk menghadapi mereka kekuatan apa saja yang kamu sanggupi dan dari kuda-kuda yang ditambat untuk berperang (yang dengan persiapan itu) kamu menggentarkan musuh Allah, musuhmu dan orang-orang selain mereka yang kamu tidak mengetahuinya; sedang Allah mengetahuinya. Apa saja yang kamu najkahkanpadajalan Allah niscaya akan dibalas dengan cukup kepadamu dan kamu tidak akan dianiaya (dirugikan). (QS.Al-Anfal :6o):</p>

        <p>Nabi menafsirkan kata kekuatan (al-Quwwah) yang dimaksud dalam ayat ini adalah memanah. Nabi pernah menyampaikannya dari atas mimbar disebutkan 3 kali, sebagaimana dinyatakan dalam satu hadits:</p>

        <p>Nabi berkata: “Dan siapkanlah untuk menghadapi mereka kekuatan apa saja yang kamu sang gupi” Ingatlah kekuatan itu adalah memanah, Ingatlah kekuatan itu adalah memanah, Ingatlah kekuatan itu adalah memanah, (HR Muslim, al-Turmudzi, Abu Dawud, Ibn Majah, Ahmad, dan al-Darimi)</p>

        <p><h3>Keempat; Anjuran Menjaga Kebersihan</h3></p>

        <p>Ajaran Islam sangat memperhatikan masalah kebersihan yang merupakan salah satu aspek penting dalam ilmu kedokteran. Dalam terminologi Islam, masalah yang berhubungan dengan kebersihan disebut dengan al-Thaharat. Dari sisi pandang kebersihan dan kesehatan, al-thaharat merupakan salah satu bentuk upaya preventif, berguna untuk menghindari penyebaran berbagai jenis kuman dan bakteri.</p>

        <p>Imam al-Suyuthi, ‘Abd al-Hamid al-Qudhat, dan ulama yang lain menyatakan, dalam Islam menjaga kesucian dan kebersihan termasuk bagian ibadah sebagai bentuk qurbat, bagian dari ta’abbudi, merupakan kewajiban, sebagai kunci ibadah, Nabi bersabda: “Dari ‘Ali ra., dari Nabi saw, beliau berkata: “Kunci shalat adalah bersuci” (HR Ibnu Majah, al-Turmudzi, Ahmad, dan al-Darimi)</p>

        <p>Berbagai ritual Islam mengharuskan seseorang melakukan thaharat dari najis, mutanajjis, dan hadats. Demikian pentingnya kedudukan menjaga kesucian dalam Islam, sehingga dalam buku-buku fikih dan sebagian besar buku hadits selalu dimulai dengan mengupas masalah thaharat, dan dapat dinyatakan bahwa ‘fikih pertama yang dipelajari umat Islam adalah masalah kesucian’.</p>

        <p>‘Abd al-Mun’im Qandil dalam bukunya al-Tadaivi bi al-Quran seperti halnya kebanyakan ulama membagi thaharat menjadi dua, yaitu lahiriah dan rohani. Kesucian lahiriah meliputi kebersihan badan, pakaian, tempat tinggal, jalan dan segala sesuatu yang dipergunakan manusia dalam urusan kehidupan. Sedangkan kesucian rohani meliputi kebersihan hati, jiwa, akidah, akhlak, dan pikiran.</p>

        <p><i>(Byutique)</i></p>';

        $article_content_2 = '';
      break;
    case '8':
      $article_id = '8';
      $article_category = 'Kesehatan';
      $article_title = 'Pentingnya Female Check-Up untuk Deteksi Dini Kanker Payudara';
      $article_image = null;

      $article_content_1 = '
        <p>Saat ini, tren penderita kanker yang menyerang wanita sudah semakin bergeser menuju wanita usia muda. Kanker tidak memandang usia, tidak harus pada wanita usia lanjut namun juga sudah mulai banyak ditemukan pada wanita usia muda. Bahkan sejak usia 13 tahun pun sudah dijumpai pasien yang terdeteksi kanker payudara. Salah satu penyebab bersarangnya kanker pada wanita usia muda adalah adanya gen bawaan yang sudah bermutasi, dimana gen tersebut hanya butuh bermutasi sebanyak satu kali lagi untuk berubah menjadi sel kanker payudara.</p>

        <p>Seiring dengan berkembangnya tren tersebut, maka pemeriksaan kesehatan yang dilakukan sedini mungkin adalah jalan yang terbaik. Yang dapat dilakukan wanita adalah benar-benar memperhatikan pada fase-fase awal pertumbuhan payudara.  Dalam hal ini khususnya adalah pemeriksaan terhadap kemungkinan mengidap kanker kandungan dan kanker payudara yang dilansir sebagai jenis kanker yang paling sering menyerang wanita. Terlebih lagi saat ini sudah banyak upaya prevensi yang dilakukan untuk mencegah terjadinya kanker, khususnya pada jenis kanker yang menyerang kandungan. Seperti yang dapat dilakukan melalui pap smear yang selanjutnya ditunjang oleh vaksinasi.</p>

        <p><h3>Ketakutan dan anggapan miring terhadap kanker</h3></p>

        <p>Permasalahan yang seringkali timbul adalah kurangnya edukasi dan munculnya ketakutan untuk memeriksakan diri bagi para wanita. Untuk mengatasi hal ini, langkah yang perlu diambil adalah menerapkan awareness bahwa apabila kanker ditemukan semenjak dini, maka pengobatannya akan lebih optimal. Artinya, wanita tidak perlu menunggu lebih lama lagi sampai sel kanker berkembang menjadi tahap lanjut. Karena apabila sudah menyebar ke organ-organ lain maka akan sangat sulit untuk disembuhkan.</p>

        <p>Persepsi lain yang seringkali muncul adalah apabila seseorang yang menderita kanker harus melakukan operasi, maka penyakitnya sudah tergolong sangat berat. Ini adalah paradigma yang keliru. Sebaliknya, apabila kanker masih bisa dioperasi maka kankernya bisa diangkat secara utuh tanpa tersisa. Sedangkan tanpa operasi, bisa saja terjadi metastase atau penyebaran sel kanker ke organ-organ lainnya. Apabila sel kanker sudah mengalami metastase, maka yang bisa dilakukan hanyalah pengobatan dengan radiasi dan kemoterapi.</p>

        <p>Hal lain yang juga memicu ketakutan untuk melakukan operasi adalah iklan-iklan di media cetak. Banyak iklan mengenai pengobatan alternatif yang menyarankan untuk menghindari operasi. Yang terjadi di sini adalah benar-benar edukasi yang terbalik. Kenyataan yang sebenarnya adalah justru dengan melakukan operasi maka masalah akan tuntas, dan tidak perlu menunggu sampai kanker mencapai stadium tinggi sehingga sudah tidak bisa dioperasi.</p>

        <p>Banyak wanita yang memiliki ketakutan akan kemungkinan pengangkatan payudara atau rahim. Padahal pengangkatan payudara atau rahim sangat mungkin tidak dilakukan apabila kanker telah terdeteksi semenjak dini. Di sinilah letak pentingnya pemeriksaan atau deteksi dini. Hal ini ditunjang dengan perkembangan teknik-teknik pembedahan yang sangat pesat. Saat ini pembedahan pada kanker payudara tidak selalu harus mengangkat seluruh payudara, dan terlebih lagi mampu membentuk payudara baru yang ukurannya bisa disesuaikan dengan keinginan. Namun, yang menjadi masalah adalah pengangkatan payudara atau rahim menyangkut banyak hal dalam kehidupan wanita, dimana payudara dan kandungan adalah sesuatu yang sangat bernilai bagi seorang wanita.</p>

        <p><h3>Kesadaran akan Pentingnya Deteksi Dini</h3></p>

        <p>Saat ini kesadaran wanita di perkotaan akan deteksi dini sudah cukup tinggi, namun hal serupa tidak terjadi di daerah-daerah pelosok atau pedesaan. Padahal Indonesia adalah negara yang sangat luas, namun sayangnya belum banyak dokter spesialis di daerah-daerah terpencil. Di sini peran media sangatlah penting untuk menumbuhkan kepedulian tersebut.</p>

        <p>Bagi wanita yang memiliki keluhan, rasa nyeri, rasa tidak nyaman pada payudara, dan menemukan benjolan di payudaranya, harus segera memeriksakan diri. Karena sangat mungkin bahwa pertanda-pertanda tersebut adalah tumor. Walaupun pada mayoritas kasus tumor keluhan awalnya jarang berupa nyeri. Namun, wanita pasti bisa lebih sensitif dan merasakan keluhan hati nuraninya. Apabila ada perasaan tidak enak, sebaiknya langsung saja diperiksakan.</p>

        <p><h3>Jenis-jenis deteksi dini Kanker Payudara</h3></p>

        <p>Deteksi dini kanker payudara terbagi menjadi beberapa jenis, diantaranya adalah pemeriksaan fisik, mammography, USG, dan petanda tumor. Jenis pemeriksaan kanker payudara yang dapat dilakukan sangat tergantung pada faktor usia. Karena jenis pemeriksaan yang dianjurkan juga berbeda-beda pada setiap golongan usia. Apabila ada keluhan pada payudara bagi wanita yang masih tergolong muda, maka tidak dianjurkan untuk melakukan mammography. Biasanya untuk golongan usia ini lebih dianjurkan menggunakan USG.</p>

        <p>Namun pada golongan usia di atas 35 tahun, dimana payudara sudah tidak kencang, maka yang disarankan untuk screening-nya adalah mammography. Untuk hasil yang lebih optimal maka setelah melakukan mammography dapat dikonfirmasi menggunakan USG. Kedua pemeriksaan ini bisa seringkali dijadikan satu paket, seperti yang terdapat di RS Premier Surabaya ini. Namun, di rumah sakit lain belum tentu kedua pemeriksaan ini disatukan sekaligus.</p>

        <p>Mammography dan USG adalah dua hal yang saling melengkapi. Ada kelainan yang mampu terdeteksi oleh USG namun tidak mampu terdeteksi oleh mammography, dan begitu juga sebaliknya. Pada wanita usia muda sekalipun, walaupun sudah dilakukan USG dan ternyata ditemukan hal-hal yang mencurigakan, maka tetap disarankan untuk melakukan mammography sebagai diagnostiknya.</p>

        <p>Penggolongan jenis umur yang sudah dijelaskan di atas berhubungan dengan screening, namun apabila terdapat kelainan maka dianjurkan untuk melakukan kedua pemeriksaan tersebut. Ada kelainan-kelainan tertentu yang tidak bisa terdeteksi oleh USG, seperti mikrokalsifikasi yakni pengapuran berukuran kecil yang mengumpul dan bersifat heterogen. Kelainan mikrokalsifikasi ini adalah salah satu tanda tumor yang bersifat ganas, yang hanya dapat terlihat oleh mammography.</p>

        <p>Namun apabila terlihat adanya benjolan pada kasus-kasus yang masih awal, maka gambaran mammography yang muncul antara kista dan tumor relatif sama, khususnya untuk tumor-tumor yang memiliki garis tepi yang bagus. Di dalamnya juga bisa terjadi pergerakan payudara yang gambarannya tidak selalu memberikan gambaran distorsi. Tetapi apabila terlihat batasan yang jelas atau bulat, kasusnya bisa saja seperti yang terjadi pada kanker payudara meduler. Apabila hal ini tidak ditunjang dan dikonfirmasi menggunakan USG, maka tidak akan terlihat mana yang berupa kista (cairan) dan mana yang berupa tumor.</p>

        <p>Ada beberapa kasus yang ditemukan pada wanita yang telah melakukan pemeriksaan, namun baru setelah 3 bulan kemudian terdeteksi bahwa wanita tersebut sudah mengidap kanker payudara dengan stadium lanjut. Hal-hal seperti inilah yang bisa terjadi apabila pemeriksaan USG tidak dibarengi dengan mammography, atau sebaliknya. Bisa saja terjadi kesalahan dalam diagnosis, yang disebabkan oleh gambaran yang rancu dan tidak terlalu jelas.</p>

        <p>Jadi, bagi wanita yang pertama kali datang dengan keluhan padda payudaranya, direkomendasikan untuk terlebih dahulu melakukan pemeriksaan fisik. Lalu bagi pasien yang datang tanpa keluhan, hal pertama yang bisa dilakukan untuk screening pertama paling tidak adalah USG. Karena USG bersifat non invasif dan bisa dilakukan kapan saja, serta tidak memandang umur dan hasilnya pun bisa diambil langsung tanpa perlu menunggu lama. Jadi, bagi dokter yang telah membaca hasil USG dan menemukan adanya indikasi kanker payudara, maka bisa segera diputuskan tindakan apa yang seharusnya dilakukan, apakah perlu mammography atau tindakan lainnya.</p>

        <p><h3>Deteksi dini Kanker Payudara</h3></p>

        <p>ntuk tes mammography hanya direkomendasikan untuk wanita dengan usia 35 tahun ke atas, karena pada wanita usia muda payudaranya akan terasa sakit apabila ditekan. Hal ini menyebabkan munculnya gambaran yang kurang jelas, karena menutupi tumor menjadi tidak kelihatan. Sedangkan pada wanita berusia 35 tahun ke atas walaupun tanpa keluhan juga sangat direkomendasikan untuk melakukan screening. Mammography biasanya mampu mendeteksi tumor pada kasus-kasus khusus dalam jangka waktu 2 tahun sebelum tumor bermanifestasi secara klinis (sebelum teraba). Jadi, pada gambaran mikrokalsifikasi apabila dirunut dan dibiarkan selama 2 tahun maka akan berupa benjolan. Namun, tumor memiliki waktu perkembangan yang berbeda-beda, rata-rata berkisar antara 8 sampai 200 hari. Pada kasus tumor yang ganas dan cepat membesar tentunya waktu yang diperlukan tidak sampai 2 tahun.</p>

        <p><h3>Waktu yang tepat untuk melakukan screening</h3></p>

        <p>Ada waktu-waktu tertentu yang merupakan waktu paling baik untuk melakukan screening, dimana gambaran yang terlihat adalah gambaran yang sangat baik. Biasanya waktu yang paling baik adalah 7 sampai 10 hari setelah menstruasi bersih. Pada waktu ini, kondisi payudara masih lentur sehingga tidak akan terasa sakit apabila ditekan, dan ditunjang juga dengan pengaruh hormon yang tidak banyak. Apabila seorang wanita sedang menstruasi, disarankan untuk tidak melakukan screening.</p>

        <p><h3>Anjuran Frekuensi screening Berdasarkan Usia</h3></p>

        <p>Dengan mempertimbangkan resikonya, anjuran untuk melakukan screening bagi wanita usia 35-50 tahun minimal 2 tahun sekali. Apabila sudah melewati usia 50 tahun, maka sangat dianjurkan untuk melakukan screening minimal 1 tahun sekali. Aturan yang berbeda juga terdapat pada pasien yang datang dengan atau tanpa keluhan. Pada pasien yang datang dengan keluhan, dan setelah dilakukan USG ditemukan ada yang mencurigakan, maka anjurannya adalah 3 bulan sekali. Dan untuk lebih amannya lagi, maka harus melakukan kontrol ke dokter setiap 6 bulan sekali.</p>

        <p><i>( Byutique team )</i></p>';

        $article_content_2 = '';
      break;
    case '9':
      $article_id = '9';
      $article_category = 'Business & Entrepreneurship';
      $article_title = 'Pengusaha Muslim harus Meningkatkan Etika dalam Berbisnis';
      $article_image = null;

      $article_content_1 = '
        <p>ISLAM mengakui kewirausahaan tidak hanya sebagai sarana untuk mencari nafkah, tetapi juga sebagai panggilan mulia yang dapat meningkatkan derajat seorang Muslim, yang setara dengan hamba Allah yang saleh. Hal ini didasarkan pada sabda Nabi Muhammad SAW:</p>

        <p>Dari Abu Sa’id Al-Khudri radhiyallahu ‘anhu, Nabi shallallahu alaihi wasallam bersabda:</p>

        <p>“Pedagang yang senantiasa jujur lagi amanah akan bersama para nabi, orang-orang yang selalu jujur dan orang-orang yang mati syahid.” (HR. Tirmidzi)</p>

        <p>Dari Rafi’ bin Khadij radhiyallahu ‘anhu, ia berkata: Ada seseorang bertanya, “Penghasilan apakah yang paling baik, Wahai Rasulullah?” Beliau jawab:</p>

        <p>“Penghasilan seseorang dari jerih payah tangannya sendiri dan setiap jual beli yang mabrur.” (HR. Ahmad di dalam Al-Musnad no.16628)</p>

        <p>Dan sejarah kehidupan Rasulullah shallallahu alaihi wasallam pun menunjukkan bahwa beliau dan sebagian para sahabatnya adalah para pedagang professional.</p>

        <p>BEBERAPA PELAJARAN PENTING DAN FAEDAH ILMIAH YANG TERKANDUNG DI DALAM HADITS-HADITS DI ATAS:</p>

        <p>1. Agama Islam agama yang paling sempurna dalam segala hal. Diantara bukti kesempurnaan agama Islam dan rahmatnya bagi alam semesta, syariatnya menganjurkan kepada umatnya agar bekerja dan berbisnis dengan jalan yang benar dan menjauhi segala hal yang dilarang oleh Allah dan rasul-Nya. Karena tiada suatu perkara pun yang dilarang oleh Allah dan rasul-Nya melainkan perkara tersebut akan mendatangkan bencana dan mudharat bagi para pelakunya.</p>

        <p>2. Berdagang merupakan salah satu profesi yang sangat mulia dan utama selagi dijalankan dengan jujur dan sesuai dengan aturan serta tidak melanggar batas-batas syari’at yang telah ditetapkan oleh Allah dan rasul-Nya di dalam Al-Qur’an dan As-Sunnah Ash-Shahihah.</p>

        <p>3. Hendaknya seorang pengusaha membekali dirinya dengan bekal keimanan dan ilmu syar’i, khususnya yg berkaitan dengan fikih muamalah dan bisnis agar bisa menjadi pengusaha yang baik dan benar serta tidak terjerumus dalam hal-hal yang haram.</p>

        <p>4. Hendaknya seorang pengusaha menghiasi dirinya dengan akhlak islami yang mulia seperti jujur, pemurah, amanah, kasih sayang, dsb, sebagaimana yg diajarkan dan dicontohkan oleh Rasulullah shallallahu alaihi wasallam.</p>

        <p>5. Seorang pengusaha hendaknya melandasi bisnis dan perniagaannya dengan niat yg baik dan ikhlas karena Allah, agar profesi yg dijalankannya mendatangkan pahala dan keridhoan dari Allah karena bernilai ibadah yg agung.</p>

        <p>6. Penghasilan yg diperoleh dari perniagaan dan pekerjaan lainnya akan mengandung berkah dan manfaat yg banyak jika diperoleh dengan jalan yg baik dan benar serta diinfaqkan dan dikeluarkan zakatnya (jika tlh terpenuhi syarat wajib zakat) dan diinfaqkan di jalan yg Allah ridhoi.</p>

        <p>7. Bisnis dan profesi apapun beserta keuntungannya akan menjadi musibah dan petaka bagi pelakunya di dalam kehidupan dunia dan akhirat jika dilakukan dengan cara-cara yg diharamkan oleh Allah dan Rasul-Nya.</p>

        <p>Dari paparan hadis Rasulullah jelaslah bahwa kewirausahaan bukanlah tentang menghasilkan uang dalam jangka pendek. Kita tidak harus membayangkan bahwa pengusaha hanya melakukan bisnis dengan tujuan untuk memperoleh banyak uang dalam jangka pendek, dan menggunakan cara apapun, etika atau tidak etis.</p>

        <p>Banyak peneliti telah memberikan bukti bahwa pengusaha yang telah mencapai sukses jangka panjang dalam membangun bisnis mereka adalah dengan cara menerapkan etika yang benar sebagai bagian integral dari bisnis mereka.</p>

        <p>Ini mungkin bertentangan dengan persepsi banyak orang yang bersikap etis hanya akan menghambat kesuksesan bisnis, terutama ketika menghadapi persaingan yang ketat di dunia bisnis.</p>

        <p>Nilai-nilai etika pada dasarnya penting dalam menjalankan bisnis karena dapat memberikan jaminan bahwa kepentingan bisnis, masyarakat dan pemangku kepentingan terlindung dengan baik dari semua elemen yang tidak beralasan seperti penipuan, eksploitasi, manipulasi, penindasan dan ketidakadilan, yang akhirnya mengakibatkan kejatuhan dari bisnis itu sendiri.</p>

        <p>Al-Quran membuat pengingat tentang menjadi jujur ​​dan dapat dipercaya, untuk melindungi orang dari melakukan penipuan dan penyimpangan, terutama dalam transaksi bisnis. Misalnya, Al-Quran menyatakan: “.......Dan sempurnakanlah takaran dan timbangan dengan adil" (Al-Quran 6: 152) "...... Dan janganlah kamu kurangi takaran dan timbangan.....” (Al-Quran 11: 84).</p>

        <p>Pengingat menandakan bahwa selalu ada kecenderungan melakukan penipuan dan penyimpangan dalam setiap transaksi bisnis, apakah sederhana atau kompleks, yang akan mengakibatkan sengketa berkepanjangan antara pihak-pihak yang terlibat.</p>

        <p>Tidak diragukan lagi, kepercayaan merupakan unsur etika yang penting di mana fondasi bisnis dibangun, dan dalam hal ini, Al-Quran menyebutkan:“Sesungguhnya Allah menyuruhmu menyampaikan amanat kepada yang berhak menerimanya..... “(Al-Quran 4: 58 ).</p>

        <p>Oleh karena itu, pengusaha Muslim harus selalu membangun fondasi untuk bisnis mereka pada kepercayaan.</p>

        <p>Sebuah contoh sederhana adalah bahwa seorang pengusaha tidak harus mengeksploitasi konsumen dengan menyediakan produk-produk berkualitas rendah atau layanan dengan harga tinggi, hanya karena ada orang lain yang menawarkan produk atau jasa serupa.</p>

        <p>Harga yang dikenakan harus mencerminkan kualitas produk yang ditawarkan atau layanan yang diberikan.</p>

        <p>Perkembangan signifikan lain dalam lanskap bisnis saat ini adalah berkembangnya transaksi bisnis melalui dunia maya, atau penggunaan Internet.</p>

        <p>Kemajuan teknologi telah membuat transaksi bisnis internet berkembang dan sebagai hasilnya, tatap muka dengan pelanggan tidak diperlukan lagi, sementara produk akan dikirimkan langsung  ke alamat pelanggan. Dalam hal ini transaksi bisnis membutuhkan tingkat kepercayaan yang lebih tinggi dibandingkan dengan cara berbisnis tradisional.</p>

        <p>Penjual harus menjaga janji mereka dalam memastikan bahwa barang yang dipesan adalah dari kualitas yang sama seperti yang diiklankan di media platform sosial mereka.</p>

        <p>Ketepatan waktu juga merupakan hal yang penting yang tidak dapat diabaikan sebagai bagian dari usaha dalam membangun fondasi bisnis yang kuat atas dasar kepercayaan.</p>

        <p>Waktu harus dikelola dengan baik dan efektif, khususnya ketika berhadapan dengan pelanggan dan pemasok. Barang atau jasa harus dikirim ke pelanggan tepat waktu setelah pembayaran dilakukan.</p>

        <p>Itulah sebabnya Islam menghargai pengusaha yang sarat dengan nilai-nilai etika sebagai setara dengan para nabi, orang-orang yang jujur ​​dan para martir.</p>

        <p>Sangat penting bahwa generasi Muslim sekarang harus termotivasi dan bercita-cita untuk menjadi pengusaha yang menganut etika sebagai kekuatan mereka, yang pada gilirannya akan memungkinkan mereka untuk berkontribusi secara efektif untuk pengembangan dan kemajuan bangsa serta masyarakat. (Jakarta, Feb16).</p>

        <p><i>Narasumber: Wempy Dyocta Koto</i></p>';

        $article_content_2 = '';
      break;
    case '10':
      $article_id = '10';
      $article_category = 'Business & Entrepreneurship';
      $article_title = 'Ekonomi Islam Pendorong Pertumbuhan Ekonomi Global';
      $article_image = null;

      $article_content_1 = '
        <p>Sebagaimana kita ketahui saat ini kondisi ekonomi di berbagai negara seluruh dunia terus mengalami ketidakpastian. Dalam ketidakpastian tersebut, ekonomi Islam muncul sebagai pendorong pertumbuhan baru. Konsumen Muslim semakin menjadi andalan ekonomi global. Pengeluaran mereka pada makanan dan gaya hidup termasuk mode halal, wisata dan media tumbuh sebesar 9,5 persen menjadi sekitar US$ 2 triliun pada tahun 2013 dari perkiraan tahun-tahun sebelumnya dan diperkirakan akan mencapai US$ 3,7 triliun pada 2019 pada tingkat pertumbuhan tahunan gabungan sebesar 10,8 persen, berdasarkan laporan Ekonomi Islam Global Thomson Reuters 2014-2015.</p>

        <p>Sebagai  tambahan,  aset keuangan Islam diprediksi meningkat dari US$ 1,66 triliun pada 2013, menjadi  US$ 3,5 triliun pada 2019, dengan tingkat pertumbuhan yang diantisipasi dari sekitar 15 persen per tahun.</p>

        <p>Keuangan Islam adalah seperti bahan bakar yang menggerakkan ekonomi Islam. Hal ini umum untuk berbagai sektor yang membentuk ekonomi Islam. Tanpa itu, ekonomi Islam tidak akan ada. Bagaimana bisa apa-apa dianggap halal, apakah itu makanan atau obat-obatan, pariwisata halal,  atau kosmetik halal, jika itu adalah produk investasi keuangan konvensional?</p>

        <p>Saat ini telah  berkembang suatu pendapat bahwa ekonomi Islam akan memiliki dampak yang signifikan pada tatanan ekonomi global. Pelanggan berbagi nilai-nilai yang mereka yakini, dan kebutuhan berdasarkan nilai adalah penggerak  yang stabil untuk semua sektor. Dengan angka pertumbuhan yang mengesankan diperkirakan selama bertahun-tahun ke depan, beragam sektor dalam pasar gaya hidup halal  yang terdapat dalam ekonomi global, didorong oleh daya beli konsumen Muslim. Budaya start-up Muslim juga muncul, yang merupakan penggerak lain untuk ekonomi Islam yang bisa membawa perubahan bagi perekonomian dunia.</p>

        <p>Disamping itu sektor keuangan Islam saat ini dianggap sektor yang paling berkembang dalam ekonomi Islam. Jumlah global lembaga keuangan Islam, termasuk bank, perusahaan takaful dan perusahaan investasi dan pembiayaan, telah melebihi 1.500 perusahaan. Penerimaan global bertumbuh, seperti persaingan antara keuangan syariah dan konvensional. Bank syariah memiliki peranan yang lebih besar dalam mengemudikan industri dengan mengembangkan produk baru yang memberi nilai tambah terhadap produk yang telah ada, yang pada gilirannya mempercepat transisi ke perbankan syariah, terutama untuk entitas non-Islam misalnya, telah ditemukan produk seperti sukuk sebagai alternatif yang layak untuk obligasi konvensional.</p>

        <p>Keuangan Islam memiliki  daya tarik yang universal sedangkan pembiayaan konvensional dijauhi oleh banyak Muslim. Selain itu keuangan Islam menawarkan cara yang lebih baik, lebih etis dalam bisnis. Sejak krisis keuangan global, kekhawatiran tentang perbankan konvensional dan cara melakukan bisnis dan produk berisiko tinggi telah meningkatkan  semangat dikalangan praktisi perbankan untuk mencari alternatif perbankan yang lebih etis. Keuangan Islam menawarkan alternatif itu.</p>

        <p>Selain  sektor keuangan Islam, peluang bisnis lain adalah  industri makanan halal,  pasar wisata halal, terutama keluarga, bisnis dan wisata mewah, dan perhotelan. Dalam dunia fashion halal, peluangnya adalah  di platform e-commerce, gerai ritel, fashion branding, waralaba dan distribusi kemitraan terutama untuk pasar Muslim Barat sebagai segmen terbesar.</p>

        <p>Pada sektor rekreasi dan media Muslim  termasuk didalamnya seperti segmen pemasaran gaya hidup, iklan digital, game, media sosial dan halal merek global. Dalam industri farmasi dan kosmetik  ada potensi besar untuk produk-produk seperti vaksin halal, bahan manufaktur, serta kemitraan eceran dan grosir.</p>

        <p>Tahun 2015 yang lalu merupakan suatu langkah penentu terhadap layanan gaya hidup Islam untuk bergerak dari membangun sebuah bisnis biasa untuk fokus membangun suatu model bisnis yang sukses dan strategi pelaksanaan yang berkualitas. Diharapkan kedepannya produk-produk berbasis gaya hidup Islam semakin maju dan dapat menjadi pemimpin pasar. (Feb2016).</p>

        <p><i>Narasumber: Wempy Dyocta Koto</i></p>';

        $article_content_2 = '';
      break;
    case '11':
      $article_id = '11';
      $article_category = 'Business & Entrepreneurship';
      $article_title = 'Merek Terkenal Internasional Mulai Memasuki Pasar Muslim Global';
      $article_image = null;

      $article_content_1 = '
        <p>Rumah mode Italia Dolce & Gabbana awal tahun 2016 ini, meluncurkan koleksi jilbab dan abaya yang menargetkan muslimah di seluruh dunia. Berita ini sempat membuat heboh media sosial. Namun, tidak berarti D&G adalah merek fashion global pertama yang melakukan hal ini. Menurut sebuah artikel di The Guardian, dalam beberapa tahun terakhir, sekelompok label mode  seperti Mango, Tommy Hilfiger, DKNY, Oscar de la Renta dan Monique Lhuillier telah menghasilkan koleksi seperti  gaun, selendang dan celana wide-leg, yang sering dijual selama bulan Ramadan. Tapi D & G mungkin yang pertama sebagai merek  fesyen global eksklusif yang memasarkan langsung jilbab dan abaya.</p>

        <p>Saat ini  tidak hanya fesyen dan makanan saja, tapi kini kita sudah dapat menemukan berbagai produk yang dikemas secara  Muslim - branding, seperti  travelling, komik, tokoh / karakter , liburan, akomodasi, medis, kosmetika, dan perbankan.</p>

        <p>Perlu saya tekankan disini bahwa membangun merek yang menarik bagi konsumen Muslim adalah dengan mengetahui perbedaan antara syariah dan halal – sebagai nilai intrinsik dalam Islam.</p>

        <p>Bagi umat Islam, syariah berarti prinsip-prinsip kehidupan sedangkan halal mengacu pada bahan-bahan tertentu dari suatu produk (misalnya, produk yang mengecualikan derivatif terkait babi, berasal dari hewan yang disembelih sesuai pedoman Islam, dan tidak mengandung alkohol).</p>

        <p>Sebagai contoh, akhir tahun lalu, maskapai syariah-compliant pertama Malaysia Rayani Air mulai beroperasi dengan penerbangan perdananya lepas landas dari Kuala Lumpur ke pulau  Langkawi. Dalam penerbangan tersebut makanan yang disajikan benar-benar halal, dan  konsumsi alkohol menjadi hal yang terlarang.  Dalam bisnis pariwisata, ada juga "halal spesialis pariwisata", yaitu halalbookings.com, diluncurkan pada tahun 2009, membantu konsumen muslim untuk menikmati liburan sesuai  tuntunan Islam. Dan ada lagi  jaringan hotel mewah Shaza, yang berbasis di Saudi Arabia, dan saat ini mereka sedang merencanakan ekspansi besar-besaran.</p>

        <p>Dibidang komik, pada tahun 2013, Marvel Comics menambahkan karakter fiksi Kamala Khan, seorang Muslim Amerika, untuk tokoh karakter nya. Sedangkan untuk bidang kesehatan, Malaysia Halal Industry Development Corporation menerima investasi $ 100 juta pada tahun 2014 untuk produksi vaksin yang dapat mengobati hepatitis, meningitis, dan penyakit meningokokus. Vaksin yang tersedia saat ini untuk penyakit ini berbahan dasar babi; sedangkan babi dilarang dalam Islam.</p>

        <p>Dan pada tahun 2014, bank milik negara terbesar di India, State Bank of India, meluncurkan Equity Fund Islam. Demikian juga, Lloyds Bank, salah satu bank komersial ternama di Inggris, menawarkan layanan perbankan syariah yang disetujui.</p>

        <p>Dari Thomson Reuters, pada 2013, pengeluaran global konsumen Muslim di sektor makanan dan gaya hidup adalah sekitar US $ 2 triliun dan diperkirakan akan mencapai US $ 3,7 triliun pada 2019.<p>

        <p>Sebagai populasi dunia terbesar  ke tiga setelah Cina dan India, populasi Muslim dunia saat ini diperkirakan 1,6 Milyar jiwa. Jika ditangani dengan benar,  pasar Muslim  merupakan potensi pasar yang sangat besar dengan peningkatan daya beli yang cepat.  Namun, sebagai pebisnis juga harus mengerti bahwa mereka juga cenderung lebih kritis dan analitis terhadap etika bisnis perusahaan besar dan menuntut dalam hal kualitas produk juga pengiriman yang tepat waktu. Jika tidak dipenuhi, mereka  itu adalah pasar yang mudah tersinggung dan sangat tidak pemaaf.</p>

        <p><i>Narasumber: Wempy Dyocta Koto</i></p>';

        $article_content_2 = '';
      break;
    case '12':
      $article_id = '12';
      $article_category = 'Business & Entrepreneurship';
      $article_title = 'Mengenal Prinsip Perbankan Syariah';
      $article_image = null;

      $article_content_1 = '
        <p>Tidak ada cara standar mendefinisikan apa yang dimaksud dengan bank Islam / bank Syariah,  adalah lembaga yang memobilisasi sumber daya keuangan dan menginvestasikannya dengan cara-cara yang ditentukan sesuai dengan ajaran Islam atau sesuai dengan tuntunan Al Qur’an dan hadist Nabi Muhammad SAW.</p>

        <p>Prinsip-prinsip keuangan Islam dipersempit ke empat konsep seperti dibawah ini:</p>

        <p><h3>1. Larangan Bunga bank atau Riba</h3></p>

        <p>Konsep pertama dan paling penting adalah bahwa baik pemberian dan penerimaan bunga dilarang keras. Hal ini umumnya dikenal sebagai riba. Ketika Riba menggerogoti seluruh sektor ekonomi, hal itu akan  membahayakan kesejahteraan semua orang dalam komunitas tersebut. Seperti yang tercantum dalam Al Qur’an, Allah Subhanu wa Ta’ala  jelas-jelas melarang praktik riba, dalam surah Al Baqarah 275 – 276 :</p>

        <p>“Orang-orang yang memakan riba tidak dapat berdiri melainkan seperti berdirinya orang yang kemasukan setan karena gila. Yang demikian itu karena mereka berkata bahwa jual beli sama dengan riba. Padahal Allah telah menghalalkan jualbeli dan mengharamkan riba. Barangsiapa mendapat peringatan dari Tuhan nya lalu dia berhenti, maka apa yang telah diperolehnya dahulu menjadi miliknya dan urusannya terserah kepada Allah. Barangsiapa mengulangi, maka mereka itu penghuni neraka, mereka kekal didalamnya.” ( QS : 2:275 )</p>

        <p>“Allah memusnahkan keuntungan riba dan melipatgandakan keuntungan sedekah. Allah tidak menyukai setiap orang yang tetap dalam kekafiran dan bergelimang dosa.” ( QS: 2:276 )</p>

        <p>Dalam Hadist sahih Muslim, Nabi Muhammad SAW, Dari Jabir ra, ia berkata, Rasulullah SAW melaknat orang-orang yang memakan harta riba, melaknat orang yang memberi makan keluarganya dengan harta riba, melaknat penulis riba, dan saksi-saksi riba. Beliau bersabda “Semua pelaku itu hukumnya sama”. (M.22:106)</p>

        <p><h3>2. Standar Etika</h3></p>

        <p>Prinsip kedua menyangkut standar etika. Ketika seorang Muslim menginvestasikan uang mereka terhadap sesuatu, secara syariah haruslah dipastikan bahwa investasi mereka adalah sesuatu yang baik dan sehat. Hal ini menunjukan bahwa investasi dalam Islam mencakup pertimbangan serius dari bisnis yang akan diinvestasikan baik dari aspek kebijakan, produk yang dihasilkannya, layanan yang disediakan, dan dampak yang dihasilkan terhadap masyarakat dan lingkungan. Dengan kata lain, umat Islam harus melihat dan terlibat langsung kedalam bisnis mereka.</p>

        <p>Dalam semua aspek dari sistem keuangan, Islam memiliki aturan-aturan tertentu, bagaimana seorang Muslim harus  terlibat dalam kegiatan ini. Misalnya, dalam perdagangan saham atau pasar sekuritas, Islam menilai apakah kegiatan perusahaan tersebut telah sesuai Syariah atau belum.</p>

        <p><h3>3. Nilai Moral dan Sosial</h3></p>

        <p>Prinsip ketiga menyangkut nilai-nilai moral dan sosial. Al-Qur\'an meminta semua penganutnya untuk berbuat baik kepada fakir miskin, seperti yang tercantum dalam Qur’an Surah Annisa ayat 36 : ....”Dan berbuat baiklah kepada kedua orang tua, karib kerabat, anak-anak yatim, orang-orang miskin..........”</p>

        <p>Lembaga keuangan Islam diharapkan untuk memberikan layanan khusus kepada mereka yang membutuhkan. Hal ini tidak terbatas pada sumbangan amal semata tetapi juga telah dilembagakan dalam industri dalam bentuk pinjaman tanpa bunga.</p>

        <p>Bisnis bank syariah meliputi proyek-proyek sosial tertentu, serta sumbangan amal. Bank syariah memberikan pinjaman bebas bunga. Sebagai contoh, jika seorang individu perlu perawatan di rumah sakit atau ingin masuk universitas, Bank Islam memberikan apa yang disebut pinjaman tanpa bunga dan biasanya diberikan untuk jangka waktu singkat satu tahun.</p>

        <p><h3>4. Kewajiban dan Risiko Bisnis</h3></p>

        <p>Prinsip terakhir menyangkut konsep menyeluruh keadilan,  bahwa semua pihak harus berbagi risiko dan keuntungan dari usaha apapun. Penyedia jasa keuangan harus bisa menerima risiko bisnis atau menyediakan beberapa layanan seperti penyediaan aset, jika dana tersebut, dari sudut pandang Syariah tidak bertentangan. Dengan menghubungkan laba dengan kemungkinan kerugian, hukum Islam membedakan keuntungan yang sah dari semua bentuk lain dari keuntungan.</p>

        <p>Dalam rangka untuk memastikan bahwa prinsip-prinsip ini diikuti, masing-masing lembaga Islam harus membangun dan menyediakan sendiri dewan penasehat yang dikenal sebagai Dewan Syariah.  Para anggota Dewan Syariah dapat mencakup bankir, pengacara atau ahli agama selama mereka dilatih dalam hukum Islam, atau Syariah. (Feb2016)</p>

        <p><i>Narasumber: Wempy Dyocta Koto</i></p>';

        $article_content_2 = '';
      break;
  }
?>
