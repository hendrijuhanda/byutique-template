<?php

  $products = array(
    array(
      'id' => '1',
      'category' => 'Fashion',
      'image' => 'product-1.jpg',
      'title' => 'Baju Muslim Brokat',
      'price' => '600',
      'discount' => 1,
      'discount_value' => 20,
      'url' => '#'
    ),
    array(
      'id' => '2',
      'category' => 'Accessories',
      'image' => 'product-2.jpg',
      'title' => 'Baju Muslim Brokat',
      'price' => '600',
      'discount' => 0,
      'discount_value' => null,
      'url' => '#'
    ),
    array(
      'id' => '3',
      'category' => 'Bags & Shoes',
      'image' => 'product-3.jpg',
      'title' => 'Ferragamo Women Shoes',
      'price' => '600',
      'discount' => 0,
      'discount_value' => null,
      'url' => '#'
    ),
    array(
      'id' => '4',
      'category' => 'Prayer Set',
      'image' => 'product-4.jpg',
      'title' => 'Sajadah Turki',
      'price' => '600',
      'discount' => 0,
      'discount_value' => null,
      'url' => '#'
    ),
    array(
      'id' => '5',
      'category' => 'Cosmetics',
      'image' => 'product-5.jpg',
      'title' => 'Stella Nude Parfume',
      'price' => '600',
      'discount' => 0,
      'discount_value' => null,
      'url' => '#'
    ),
    array(
      'id' => '6',
      'category' => 'Electronics',
      'image' => 'product-6.jpg',
      'title' => 'I Phone 6',
      'price' => '600',
      'discount' => 0,
      'discount_value' => null,
      'url' => '#'
    ),
    array(
      'id' => '7',
      'category' => 'Baby Products',
      'image' => 'product-7.jpg',
      'title' => 'Batmobile Babywalker',
      'price' => '600',
      'discount' => 1,
      'discount_value' => 50,
      'url' => '#'
    ),
    array(
      'id' => '8',
      'category' => 'Hobbiess',
      'image' => 'product-8.jpg',
      'title' => 'Golden Stories Abu-Bakr As-sideeq',
      'price' => '600',
      'discount' => 0,
      'discount_value' => null,
      'url' => '#'
    ),
  );
 ?>
