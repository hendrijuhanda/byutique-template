<?php

  $prayer_date = 'Jakarta, Senin 2/02/2016<br> 8:00 pm';
  $prayer_fajr = '3:51 am';
  $prayer_dhuhr = '11:13 am';
  $prayer_asr = '2:28 pm';
  $prayer_maghreb = '5:25 pm';
  $prayer_isha = '7:15 pm';

  $doa_title = 'Doa Memohon Dipelihara dari Siksa Neraka';
  $doa_content = '(اَللّهُمَّ قِنِيْ عَدَابَكَ يَوْمَ تَبْعَثُ عِبَادَكَ (3<br> ALLAHUMMA QINII ‘ADZAABAKA YAUMA TAB’ATSU ‘IBAADAK. (3X) <br> Artinya: “Ya Allah, peliharalah diriku dari siksa-Mu pada saat Engkau bangkitkan hamba-hamba-Mu.” (3X) (HR. Abu Dawud dan Tirmidzi)';

  $hadist_content = '1342.  Ali r.a. berkata: "Nabi saw memberiku hadiah kain dari sutra, lalu aku memakainya, tiba-tiba aku melihat kemarahan di wajah Nabi saw., lalu  aku bagi-bagikan kepada istriku." (Bukhari, Muslim) ';

?>
