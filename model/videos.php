<?php

  $videos = array(
    array (
      'id' => '1',
      'image' => 'video-1.jpg',
      'title' => 'Exclusive Interview with Dewi Sandra',
      'url' => '#'
    ),
    array (
      'id' => '2',
      'image' => 'video-2.jpg',
      'title' => 'Seminar Muslimah Indonesia Bersama Wardah',
      'url' => '#'
    ),
    array (
      'id' => '3',
      'image' => 'video-3.jpg',
      'title' => 'Tutorial Hijab Segi Empat',
      'url' => '#'
    ),
    array (
      'id' => '4',
      'image' => 'video-4.jpg',
      'title' => 'Exclusive Interview with Dian Pelangi',
      'url' => '#'
    )
  );

?>
