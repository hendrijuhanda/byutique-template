<?php

  include 'model/sliders.php';
  include 'model/articles.php';

  if ( isset( $_GET['article'] ) ) include 'model/example.php';
?>

<!-- Main Content -->
<main id="main-content" class="main-content article-content">

  <!-- Breadcrumb -->
  <ul class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li><a href="#">Business &amp; Entrepreneurship</a></li>
    <li class="active">Keuangan</li>
  </ul>

  <!-- Sub Nav -->
  <nav class="article-content--sub-nav">
    <h2 class="heading">Business &amp; Entrepreneurship</h2>

    <ul class="list-unstyled">
      <li class="active">Keuangan</li>
      <li><a href="#">Wirausaha</a></li>
      <li><a href="#">Ekonomi</a></li>
      <li><a href="#">Tips Wirausaha</a></li>
      <li><a href="#">Tokoh Inspirasional</a></li>
    </ul>
  </nav>

  <!-- Article -->
  <article id="article-<?php echo $article_id; ?>" class="article-content--main-article">
    <h3 class="article-category"><?php echo $article_category; ?></h3>
    <h2 class="article-title"><?php echo $article_title; ?></h2>

    <?php if ( $article_image !== null ) : ?>
      <img class="article-image" src="<?php echo $article_image; ?>" alt="">
    <?php endif; ?>

    <div class="article-content part-1">
      <?php echo $article_content_1; ?>
    </div>

    <div class="article-content part-2">
      <?php echo $article_content_2; ?>
    </div>
  </article>

</main>
<!-- /Main Content -->
