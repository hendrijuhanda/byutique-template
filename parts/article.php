<?php

  include 'model/sliders.php';
  include 'model/articles.php';

?>

<!-- Main Content -->
<main id="main-content" class="main-content article-content">

  <!-- Breadcrumb -->
  <ul class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li class="active">Business &amp; Entrepreneurship</li>
  </ul>

  <!-- Sub Nav -->
  <nav class="article-content--sub-nav">
    <h2 class="heading">Business &amp; Entrepreneurship</h2>

    <ul class="list-unstyled">
      <li><a href="#">Keuangan</a></li>
      <li><a href="#">Wirausaha</a></li>
      <li><a href="#">Ekonomi</a></li>
      <li><a href="#">Tips Wirausaha</a></li>
      <li><a href="#">Tokoh Inspirasional</a></li>
    </ul>
  </nav>

  <!-- Slider -->
  <section class="article-content--slider section-wrap">

    <div class="row">
      <div class="col-md-9">

        <div class="slider-wrap">
          <div class="slider">
            <?php foreach ( $article_slider as $slider ) :

              $slider_id = $slider['id'];
              $slider_image = 'assets/img/article-slider/' . $slider['image'];
              $slider_category = $slider['category'];
              $slider_title = $slider['title'];

            ?>

              <div id="slider-<?php echo $slider_id; ?>" class="slider-item">
                <img src="<?php echo $slider_image; ?>" alt="">
                <div id="slider-<?php echo $slider_id; ?>-category"><?php echo $slider_category; ?></div>
                <div id="slider-<?php echo $slider_id; ?>-title"><?php echo $slider_title; ?></div>
              </div>

            <?php endforeach; ?>
          </div>

          <h3 class="slider-category">Wira Usaha</h3>
          <h4 class="slider-title">Mengenal Prinsip Perbankan Syariah</h4>
        </div>

      </div>

      <div class="col-md-3">

        <!-- Featured Article -->
        <div class="article-content--featured-article">

          <?php foreach ( $featured_articles as $article ) :

            $article_id = $article['id'];
            $article_image = 'assets/img/featured-articles/' . $article['image'];
            $article_category = $article['category'];
            $article_title = $article['title'];
            $article_url = $article['url'];

          ?>

            <a href="<?php echo $article_url; ?>" id="featured-<?php echo $article_id; ?>" class="article-wrap featured">
              <img class="article-image" src="<?php echo $article_image; ?>" alt="">

              <h3 class="article-category"><?php echo $article_category; ?></h3>
              <h4 class="article-title"><?php echo $article_title; ?></h4>
            </a>

          <?php endforeach; ?>

        </div>
      </div>
    </div>

  </section>

  <!-- Latest News -->
  <section class="article-content--latest-news section-wrap">
    <h2 class="heading"><span>Latest News</span></h2>

    <div class="row">
      <div class="col-md-9">

        <div class="row">

          <?php foreach ( $latest_articles as $article ) :

            $article_id = $article['id'];
            $article_image = 'assets/img/latest-articles/' . $article['image'];
            $article_category = $article['category'];
            $article_title = $article['title'];
            $article_url = $article['url'];

          ?>

            <div class="col-md-4 col-item">
              <div id="article-<?php echo $article_id; ?>" class="article-wrap latest-news">
                <img class="article-image" src="<?php echo $article_image; ?>" alt="">

                <ul class="list-unstyled">
                  <li><a href="#" class="article-category"><?php echo $article_category; ?></a></li>
                  <li><a href="#" class="article-title"><?php echo $article_title; ?></a></li>
                </ul>
              </div>
            </div>

          <?php endforeach; ?>

        </div>

        <div class="text-center">
          <a href="#" class="more-button">More News</a>
        </div>

      </div>

    </div>
  </section>

</main>
<!-- /Main Content -->
