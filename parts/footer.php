<footer id="master-footer" class="master-footer">
  <div class="master-footer--top-side">
    <div class="container">
      <div class="inner">

        <!-- Footer Logo -->
        <div class="master-footer--footer-logo">
          <img src="assets/img/footer-logo.png" alt="">
        </div>

        <!-- Footer Text -->
        <p>Byutique.com adalah e-commerce yang didedikasikan untuk wanita muslimah, Byutique menyediakan pakaian muslim dengan gaya yang fresh dan fashionable. Kami menawarkan ragam pilihan busana muslim; mulai dari pakaian bergaya basic seperti dress, atasan, rok, tunik, hingga scarf dan aksesori. Produk-produk yang kami sediakan dipilih secara selektif untuk memenuhi kebutuhan fashion muslimah bergaya chic dan up to date, semuanya ditawarkan dengan harga yang terjangkau. Selain bertujuan menjadi e-commerce busana muslim terbaik di Indonesia, Byutique.com juga ingin menjadikan para Hijabi Indonesia untuk menjadi trendsetter bagi fashion muslim dunia.</p>

        <p>Daftarkan juga email Anda ke newsletter dan follow social media Byutique.com untuk menjadi pertama yang tahu mengenai promo spesial dan potongan harga. Nikmati juga layangan pengiriman dan retur gratis untuk kepuasan belanja online Anda di Byutique.com.</p>

      </div>
    </div>
  </div>

  <div class="master-footer--bottom-side">
    <div class="container">
      <div class="row">
        <div class="col-md-offset-2 col-md-10">

          <!-- Footer Nav -->
          <nav class="master-footer--footer-nav">
            <ul class="list-unstyled">
              <li><a href="#">Business &amp; Entrepreneurship</a></li>
              <li><a href="#">Empowerment</a></li>
              <li><a href="#">Health</a></li>
              <li><a href="#">Lifestyle</a></li>
              <li><a href="#">Parenting</a></li>
              <li><a href="#">Spiritual</a></li>
            </ul>
          </nav>
        </div>
      </div>

      <div class="row">
        <div class="col-md-offset-2 col-md-2">

          <!-- Footer Info -->
          <div class="inner master-footer--footer-info">
            <h4>Information</h4>

            <ul class="list-unstyled">
              <li><a href="#">Tentang Kami</a></li>
              <li><a href="#">Syarat dan Ketentuan</a></li>
              <li><a href="#">Kebijakan Privasi</a></li>
            </ul>
          </div>

        </div>

        <div class="col-md-2">

          <!-- Footer Help -->
          <div class="inner master-footer--footer-help">
            <h4>Help / Bantuan</h4>

            <ul class="list-unstyled">
              <li><a href="#">Cash on Delivery</a></li>
              <li><a href="#">FAQ</a></li>
              <li><a href="#">Cara Pemesanan</a></li>
              <li><a href="#">Proses Pengembalian</a></li>
              <li><a href="#">Proses Pengiriman</a></li>
            </ul>
          </div>

        </div>

        <div class="col-md-2">

          <!-- Footer Sitemap -->
          <div class="inner master-footer--footer-sitemap">
            <h4>Sitemap</h4>

            <ul class="list-unstyled">
              <li><a href="#">Cash on Delivery</a></li>
              <li><a href="#">FAQ</a></li>
              <li><a href="#">Cara Pemesanan</a></li>
              <li><a href="#">Proses Pengembalian</a></li>
              <li><a href="#">Proses Pengiriman</a></li>
            </ul>
          </div>

        </div>

        <div class="col-md-4">

          <!-- Footer Newsletter -->
          <div class="inner master-footer--footer-newsletter">
            <h4>Daftar Newsletter</h4>

            <div class="form-group">
              <input type="email" class="form-control" placeholder="Masukan email Anda">
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>

  <div class="master-footer--copyright">
    <div class="container">
      All Rights Reserved &copy; 2016 byutique.com
    </div>
  </div>
</footer>
