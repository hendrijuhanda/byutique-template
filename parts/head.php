<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<title>Byutique - </title>

  <!-- Stylesheet -->
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,700,700italic|Arimo' rel='stylesheet' type='text/css'>
	<link href="assets/components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/components/jquery-ui/themes/base/jquery-ui.min.css" rel="stylesheet">
	<link href="assets/components/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/components/slick.js/slick/slick.css" rel="stylesheet">
	<link href="assets/style.css" rel="stylesheet">

	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

  <!--[if lt IE 9]>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
