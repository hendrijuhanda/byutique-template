<!-- Master Header -->
<header id="master-header" class="master-header">
  <div class="master-header--top-bar"></div>

  <div class="container master-header--mid-bar">
    <div class="row">
      <div class="col-md-3">

        <!-- Logo -->
        <a href="#" class="master-header--logo"><img src="assets/img/logo.png"></a>

      </div>

      <div class="col-md-6">

        <!-- Search Form -->
        <div class="master-header--search-form">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
              <button class="btn btn-primary" type="button">Search</button>
            </span>
          </div>
        </div>

      </div>

      <div class="col-md-3 right-column">

        <div class="inner">
          <!-- Language -->

          <!-- User Sign -->
          <ul class="list-unstyled master-header--user-sign">
            <li><a href="#">Log In</a></li>
            <li>/</li>
            <li><a href="#">Register</a></li>
          </ul>
        </div>

        <!-- Links -->
        <ul class="list-unstyled master-header--links">
          <li><i class="fa fa-heart"></i> <a href="#">Wishlist</a></li>
          <li><i class="fa fa-shopping-bag"></i> <a href="#">Shop (0)</a></li>
        </ul>

      </div>
    </div>
  </div>

  <!-- Main Nav -->
  <nav class="master-header--main-nav">
    <div class="container">
      <ul class="list-unstyled">
        <li><a href="#">Business &amp; Entrepreneurship</a></li>
        <li><a href="#">Empowerment</a></li>
        <li><a href="#">Health</a></li>
        <li><a href="#">Lifestyle</a></li>
        <li><a href="#">Parenting</a></li>
        <li><a href="#">Spiritual</a></li>
        <li><a href="#">Products</a></li>
        <li><a href="#">Video</a></li>
      </ul>
    </div>
  </nav>
</header>
<!-- /Master Header -->
