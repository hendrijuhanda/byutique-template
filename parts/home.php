<?php

  include 'model/sliders.php';
  include 'model/articles.php';
  include 'model/videos.php';
  include 'model/products.php';

?>

<!-- Main Content -->
<main id="main-content" class="main-content home-content">

  <!-- Slider -->
  <section class="home-content--slider section-wrap">
    <header>
      <div class="main-date">
        <?php echo date('l, d / m / Y'); ?>
      </div>

      <div class="greet">
        السَّلاَمُ عَلَيْكُمْ وَرَحْمَةُ اللهِ وَبَرَكَاتُهُ
      </div>
    </header>

    <div class="slider-wrap">
      <div class="slider">
        <?php foreach ( $main_slider as $slider ) :

          $slider_id = $slider['id'];
          $slider_image = 'assets/img/main-slider/' . $slider['image'];
          $slider_category = $slider['category'];
          $slider_title = $slider['title'];

        ?>

          <div id="slider-<?php echo $slider_id; ?>" class="slider-item">
            <img src="<?php echo $slider_image; ?>" alt="">
            <div id="slider-<?php echo $slider_id; ?>-category"><?php echo $slider_category; ?></div>
            <div id="slider-<?php echo $slider_id; ?>-title"><?php echo $slider_title; ?></div>
          </div>

        <?php endforeach; ?>
      </div>

      <h3 class="slider-category">Lifestyle</h3>
      <h4 class="slider-title">Gaya Hidup Muslimah Modern Masa Kini</h4>
    </div>

  </section>
  <!-- /Slider -->

  <!-- Latest News -->
  <section class="home-content--latest-news section-wrap">
    <div class="row">

      <?php foreach ( $latest_news as $article ) :

        $article_id = $article['id'];
        $article_image = 'assets/img/latest-news/' . $article['image'];
        $article_category = $article['category'];
        $article_title = $article['title'];
        $article_url = $article['url'];

      ?>

        <div class="col-md-3 col-item">
          <div id="article-<?php echo $article_id; ?>" class="article-wrap latest-news">
            <img class="article-image" src="<?php echo $article_image; ?>" alt="">

            <ul class="list-unstyled">
              <li><a href="#" class="article-category"><?php echo $article_category; ?></a></li>
              <li><a href="#" class="article-title"><?php echo $article_title; ?></a></li>
            </ul>
          </div>
        </div>

      <?php endforeach; ?>

    </div>
  </section>

  <!-- Most Viewed -->
  <section class="home-content--most-viewed section-wrap">
    <h2 class="heading"><span>Most Viewed</span></h2>

    <div class="row">

      <div class="col-md-8">

        <div class="row">

          <?php foreach ( $most_view as $article ) :

            $article_id = $article['id'];
            $article_image = 'assets/img/most-view/' . $article['image'];
            $article_category = $article['category'];
            $article_title = $article['title'];
            $article_url = $article['url'];

          ?>

            <div class="col-md-6 col">
              <div id="article-<?php echo $article_id; ?>" class="article-wrap most-viewed">
                <img class="article-image" src="<?php echo $article_image; ?>" alt="">

                <ul class="list-unstyled">
                  <li><a href="#" class="article-category"><?php echo $article_category; ?></a></li>
                  <li><a href="#" class="article-title"><?php echo $article_title; ?></a></li>
                </ul>
              </div>
            </div>

          <?php endforeach; ?>

        </div>

      </div>

      <div class="col-md-4">

        <!-- Ads Space -->
        <div class="home-content--ads-space">
          <img src="assets/img/ads-banner.jpg" alt="">
        </div>
      </div>

    </div>

  </section>
  <!-- /Most Viewed -->

  <!-- Video Section -->
  <section class="home-content--video-section section-wrap">
    <h2 class="heading"><span>Video</span></h2>

    <div class="row">

      <?php foreach ( $videos as $video ) :

        $video_id = $video['id'];
        $video_url = $video['url'];
        $video_image = 'assets/img/videos/' . $video['image'];
        $video_title = $video['title'];

      ?>

        <div class="col-md-3 col-item">
          <div id="video-<?php echo $video_id; ?>" class="video-wrap">
            <a href="<?php echo $video_url; ?>" class="middle-wrap">
              <img class="video-image" src="<?php echo $video_image; ?>" alt="">
              <span class="video-icon"></span>
            </a>

            <h4 class="video-title"><?php echo $video_title; ?></h4>
          </div>
        </div>

      <?php endforeach; ?>
    </div>

  </section>
  <!-- /Video Section -->

  <!-- Products Section -->
  <section class="home-content--product-section section-wrap">
    <h2 class="heading"><span>Products</span></h2>

    <div class="row">

      <?php foreach ( $products as $product ) :

        $product_id = $product['id'];
        $product_category = $product['category'];
        $product_image_url = 'assets/img/products/' . $product['image'];
        $product_title = $product['title'];
        $product_price = $product['price'];
        $product_discount = $product['discount'];
        $product_discount_value = $product['discount_value'];
        $product_url = $product['url'];

        if ( $product_discount ) {
          $product_discount_class = 'has-discount';
        }
        else {
          $product_discount_class = null;
        }

      ?>

        <div class="col-md-3">
          <div id="product-<?php echo $product_id; ?>" class="product-wrap <?php echo $product_discount_class; ?>">
            <h3 class="product-category"><?php echo $product_category; ?></h3>

            <div class="middle-wrap">
              <img class="product-image" src="<?php echo $product_image_url; ?>" alt="">

              <?php if ( $product_discount ) : ?>
                <div class="product-discount"><?php echo $product_discount_value . '%'; ?></div>
              <?php endif; ?>
            </div>

            <div class="bottom-wrap">
              <h4 class="product-title"><span><?php echo $product_title; ?></span></h4>
              <div class="product-price"><?php echo 'IDR ' . $product_price; ?></div>

              <ul class="list-unstyled">
                <li><a href="<?php echo $product_url; ?>" class="product-detail">Lihat Product</a></li>
                <li><a href="#" class="product-buy">Beli</a></li>
              </ul>
            </div>
          </div>
        </div>

      <?php endforeach; ?>

    </div>
  </section>

  <!-- Foot Banner -->
  <div class="home-content--footer-banner">
    <img src="assets/img/footer-banner.jpg" alt="">
  </div>

</main>
<!-- /Main Content -->
