<?php include 'model/sidebar.php'; ?>

<!-- Sidebar -->
<aside id="sidebar-content" class="sidebar-content">

  <!-- Prayer -->
  <section class="sidebar-content--prayer">
    <h3 class="heading">Indonesia Prayer</h3>

    <div class="prayer-search">
      <form id="prayer-search-form">
        <div class="form-group">
          <label for="search-city">Search City</label>
          <select class="form-control">
            <option value="1">Jakarta</option>
            <option value="2">Bandung</option>
          </select>
        </div>
      </form>
    </div>

    <div class="prayer-date"><?php echo $prayer_date; ?></div>

    <ul class="list-unstyled prayer-list">
      <li>Fajr / Shubuh <span><?php echo $prayer_fajr; ?></span></li>
      <li>Dhuhr / Zuhur <span><?php echo $prayer_dhuhr; ?></span></li>
      <li>Asr / Ashar <span><?php echo $prayer_asr; ?></span></li>
      <li>Maghreb / Maghrib <span><?php echo $prayer_maghreb; ?></span></li>
      <li>Isha / Isya <span><?php echo $prayer_isha; ?></span></li>
    </ul>
  </section>

  <!-- Doa -->
  <section class="sidebar-content--doa">
    <h3 class="heading">Doa Hari Ini</h3>

    <div class="doa-content">
      <h4 class="doa-title"><?php echo $doa_title; ?></h4>

      <?php echo $doa_content; ?>
    </div>
  </section>

  <!-- Hadist -->
  <section class="sidebar-content--hadist">
    <h3 class="heading">Hadist</h3>

    <div class="hadist-content"><?php echo $hadist_content; ?></div>
  </section>

  <!-- Other Articles -->
  <section class="sidebar-content--other-articles">
    <h3 class="heading">Berita Lainnya</h3>

    <?php foreach ( $other_articles as $article ) :

      $article_id = $article['id'];
      $article_image = 'assets/img/other-articles/' . $article['image'];
      $article_category = $article['category'];
      $article_title = $article['title'];
      $article_url = $article['url'];

    ?>

      <div id="article-<?php echo $article_id; ?>" class="article-wrap other-articles">
        <img class="article-image" src="<?php echo $article_image; ?>" alt="">

        <ul class="list-unstyled">
          <li><a href="#" class="article-category"><?php echo $article_category; ?></a></li>
          <li><a href="#" class="article-title"><?php echo $article_title; ?></a></li>
        </ul>
      </div>

    <?php endforeach; ?>

  </section>

  <!-- Ads Space -->
  <section class="sidebar-content--ads-space">
    <img src="assets/img/ads-banner.jpg" alt="">
  </section>

</aside>
<!-- /Sidebar -->
